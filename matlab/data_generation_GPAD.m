%% This function generates a system with different terminal functions and constraints but
% with same size. The constraint are preconditioned accodingly.
clear all;
close all;
clc;
Nm=6; % Number of masses
T_sampling=0.25;
sys_no_precond=system_generation(Nm,struct('Ts',T_sampling,'xmin', ...
    -5*ones(2*Nm,1), 'xmax', 5*ones(2*Nm,1), 'umin', -1*ones(Nm,1),'umax',...
    1*ones(Nm,1)));
various_predict_horz=10;%prediction horizon
Test_points=1;
x_rand=2*(1.8*rand(sys_no_precond.nx,Test_points)-0.9);
result.u=zeros(sys_no_precond.nu,Test_points);
time_gurob=cell(Test_points,1);
time_quad=cell(Test_points,1);
%% Generation of tree
scenario_size=[5 4 3];
for N_prb_steps=3:length(scenario_size)
    for no_of_pred=1:length(various_predict_horz)
        sys_no_precond.Np=various_predict_horz(no_of_pred);
        ops.N=sys_no_precond.Np;
        ops.brch_ftr=ones(ops.N,1);
        ops.brch_ftr(1:N_prb_steps)=scenario_size(1:N_prb_steps);
        Ns=prod(ops.brch_ftr);
        ops.nx=sys_no_precond.nx;
        ops.prob=cell(ops.N,1);
        for i=1:ops.N
            if(i<=N_prb_steps)
                pd=rand(1,ops.brch_ftr(i));
                if(i==1)
                    ops.prob{i,1}=pd/sum(pd);
                    pm=1;
                else
                    pm=pm*scenario_size(i-1);
                    ops.prob{i,1}=kron(ones(pm,1),pd/sum(pd));
                end
            else
                ops.prob{i,1}=ones(1,Ns);
            end
        end
        tic
        Tree=tree_generation(ops);
        time.tree_formation=toc;
        
        SI=scenario_index(Tree);%calculation of the scenario index.
        %%
        %Cost function
        V.Q=eye(sys_no_precond.nx);
        V.R=eye(sys_no_precond.nu);
        %%terminal constraints
        sys_no_precond.Ft=cell(Ns,1);
        sys_no_precond.gt=cell(Ns,1);
        V.Vf=cell(Ns,1);
        
        r=1*rand(Ns,1);
        sys_no_precond.trm_size=(2*sys_no_precond.nx)*ones(Ns,1);
        for i=1:Ns
            %constraint in the horizon
            sys_no_precond.Ft{i}=[eye(sys_no_precond.nx);-eye(sys_no_precond.nx)];
            sys_no_precond.gt{i}=(3+0.1*rand(1))*ones(2*sys_no_precond.nx,1);
            nt=size(sys_no_precond.Ft{i},1);
            P=Polyhedron('A',sys_no_precond.Ft{i},'b',sys_no_precond.gt{i});
            if(isempty(P))
                error('Polyhedron is empty');
            end
            V.Vf{i}=dare(sys_no_precond.A,sys_no_precond.B,r(i)*V.Q,r(i)*V.R);
        end   
        %% preconditioning the system and solve the system using dgpad.
        [sys,Hessian_app]=calculate_diffnt_precondition_matrix(sys_no_precond,V,Tree...
            ,struct('use_cell',1,'use_hessian',0));
        tic;
        Ptree=GPAD_dynamic_formulation_precondition(sys,V,Tree);
        toc
        ops_GPAD.steps=500;
        ops_GPAD.primal_inf=1e-3;
        ops_GPAD.dual_gap=10e-4;
        ops_GPAD.alpha=1/calculate_Lipschitz(sys,V,Tree);
        max_size=zeros(Test_points,length(Tree.stage));
    end
end


create_filesdata_sameFt
create_simpleheader