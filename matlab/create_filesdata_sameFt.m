% Where to store the header file:
%create the files which store the data 
tic
ptree.d=cell(1,length(Tree.children));
for i=1:length(Tree.children)
    ptree.d{1,i}=Ptree.d{1,i}';
    ptree.h{1,i}=Ptree.h{1,i}';
    if(i<=(length(Tree.children)-Ns))
        ptree.f{1,i}=Ptree.f{1,i}';
    else
        ptree.f{1,i}=Ptree.f{1,i}';
    end
end
size_of_FN = zeros(length(sys.Ft),1);
size_of_FN_cum = zeros(size(size_of_FN));
for kk=1:length(sys.Ft),
    size_of_FN(kk) = length(sys.gt{kk});
    if (kk>1), size_of_FN_cum(kk) = size_of_FN_cum(kk-1)+size_of_FN(kk-1); end
end
FN_numel = sum(size_of_FN)*sys.nx;
GN_numel = sum(size_of_FN);

ny=size(sys.F{1},1);
Nd=length(Tree.children);
Ns=length(Tree.leaves);
g=zeros(ny*(Nd-Ns),1);
for i=1:Nd-Ns
    g((i-1)*ny+1:i*ny,1)=sys.g{i};
end
filedata={
    {'Data_files/GPAD_FN.h',               'Ft',           sys.Ft,                 FN_numel}
    {'Data_files/GPAD_gN.h',               'gN',          sys.gt,                 GN_numel}
    {'Data_files/GPAD_K_GAIN.h',  'GPAD_K_GAIN',          Ptree.K,                sys.nu*sys.nx*Nd}
    {'Data_files/GPAD_PHI.h',        'GPAD_PHI',          Ptree.Phi,              Nd*ny*sys.nu}
    {'Data_files/GPAD_THETA.h',    'GPAD_THETA',          Ptree.Theta,            (Nd-Ns)*sys.nu*sys.nx+GN_numel*sys.nu}
    {'Data_files/GPAD_D.h',            'GPAD_D',          ptree.d,                Nd*ny*sys.nx}
    {'Data_files/GPAD_H.h',            'GPAD_H',          ptree.h,                Nd*sys.nu*sys.nx}
    {'Data_files/GPAD_F.h',            'GPAD_F',          ptree.f,                (Nd-Ns)*sys.nx*sys.nx+GN_numel*sys.nx}
    {'Data_files/GPAD_Fc.h',                'F',            sys.F,                 ny*sys.nx*(Nd-Ns)}
    {'Data_files/GPAD_Gc.h',                'G',            sys.G,                 ny*sys.nu*(Nd-Ns)}
    {'Data_files/GPAD_g.h',                 'g',                g,                 ny*(Nd-Ns)}
    };

for kk=1:11
    m_matrix =filedata{kk};
    f = fopen(filedata{kk,1}{1,1},'w+');
    fprintf(f,'%d \n',filedata{kk,1}{1,4});
    m = m_matrix{3};
    if isnumeric(m), % m is a matrix
        for i=1:size(m,2),
            for j=1:size(m,1),
                fprintf(f,'%g\n', m(j,i));
            end
        end
    elseif iscell(m), % m is a cell
        for s=1:length(m),
            mm=m{s};
            for i=1:size(mm,2),
                for j=1:size(mm,1),
                    fprintf(f,'%g\n', mm(j,i));
                end
            end
        end
    end
    fclose(f);
end


