/*
 * forward_substitution_testing.cuh
 *
 *  Created on: Oct 25, 2014
 *      Author: ajay
 */

#ifndef FORWARDSUBSTITUTION_CUH_
#define FORWARDSUBSTITUTION_CUH_

#include<curand.h>
#include"cublas_v2.h"
#include "cuda_timer.cuh"

#define NX_t 40
#define NU_t 30

//typedef unsigned int uint_t;
//typedef double real_t;
/*
 * Here we check the correctness and benchmark the backward substitution and forward substitution.
 * Both those stages are in dynamic_GPAD() algorithm.
 */
template<typename T>__global__ void summation_child_test(T *x,T *y,T *value,uint_t* ancestor);
template<typename T>__global__ void parallel_sum_test(T *a,const T *b,int size);
template<typename T>__host__ void control_calculate_test(const T *kgain,const T* x,const int node, T* u,cublasHandle_t handle);
template<typename T>__host__ void system_update_test(T *x,T *u,T *lambda,
		T *matA,T *matB,T *value, uint_t* ancestor,int node,int cum_nchild,cublasHandle_t handle);
__host__ void forward_substitution_test(void);


/*implementation*/
__host__ void forward_substitution_test(void){
	real_t* x;
	real_t* u;
	real_t* value;
	real_t* kgain;
	real_t* matA;
	real_t* matB;
	real_t* lambda;
	real_t t;


	uint_t nodes=300;
	uint_t x_size=NX_t;
	uint_t u_size=NU_t;
	const uint_t no_child_rep=3;
	const uint_t rep_child[no_child_rep]={15,25,5};
	uint_t nchild[nodes];
	uint_t cum_nchild[nodes];

	int l=0;
	for(int i=0;i<nodes;i++){
		nchild[i]=rep_child[l];
		if(i==0)
			cum_nchild[i]=rep_child[l];
		else
			cum_nchild[i]=cum_nchild[i-1]+rep_child[l];
		if(l==no_child_rep-1)
			l=0;
		else
			l=l+1;
	}

	x=(real_t*)malloc(x_size*nodes*sizeof(*x)+x_size*cum_nchild[nodes-1]*sizeof(*x));
	u=(real_t*)malloc(u_size*nodes*sizeof(*u));
	lambda=(real_t*)malloc(x_size*nodes*sizeof(*lambda));
	kgain=(real_t*)malloc(u_size*x_size*sizeof(*kgain));
	matA=(real_t*)malloc(x_size*x_size*sizeof(*matA));
	matB=(real_t*)malloc(x_size*u_size*sizeof(*matB));
	//Initialization of x, u;
	for(int i=0;i<nodes;i++){
		for(int k=0;k<x_size;k++)
			x[i*x_size+k]=1;
		for(int k=0;k<u_size;k++)
			u[i*u_size+k]=0.5;
	}


	//gain (control-state K) matrix, matrix A and matrix B;
	for(int i=0;i<x_size;i++){
		for(int k=0;k<x_size;k++){
			matA[i*x_size+k]=0.1;
		}
	}
	for(int i=0;i<x_size*u_size;i++){
		kgain[i]=1;
	}
	for(int i=0;i<u_size;i++){
		for(int k=0;k<x_size;k++){
			matB[i*u_size+k]=0.2;
		}
	}
	value=(real_t*)malloc(x_size*cum_nchild[nodes-1]*sizeof(*value));
	uint_t ancestor[cum_nchild[nodes-1]];

	for(int i=0;i<nodes;i++){
		for(int j=0;j<nchild[i];j++){
			if(i==0){
				ancestor[j]=i;
				for(int k=0;k<x_size;k++)
					value[j*x_size+k]=nchild[i];
			}
			else{
				ancestor[cum_nchild[i-1]+j]=i;
				for(int k=0;k<x_size;k++)
					value[(cum_nchild[i-1]+j)*x_size+k]=nchild[i];
			}
		}
	}
	//start the event
	start_tictoc();

	//dev_data
	real_t* dev_x=NULL;
	//real_t* dev_y=NULL;
	real_t* dev_kgain=NULL;
	real_t* dev_matA=NULL;
	real_t* dev_matB=NULL;
	real_t* dev_value=NULL;
	real_t* dev_u=NULL;
	real_t* dev_lambda=NULL;
	uint_t* dev_cum_child=NULL;
	uint_t* dev_nchild=NULL;
	uint_t* dev_ancestor=NULL;


	_CUDA(cudaMalloc((void**)&dev_x, x_size*nodes*sizeof(*x)+x_size*cum_nchild[nodes-1]*sizeof(*x)));
	//_CUDA(cudaMalloc((void**)&dev_y,x_size*cum_nchild[nodes-1]*sizeof(*dev_y)));
	_CUDA(cudaMalloc((void**)&dev_cum_child,nodes*sizeof(uint_t)));
	_CUDA(cudaMalloc((void**)&dev_nchild,nodes*sizeof(uint_t)));
	_CUDA(cudaMalloc((void**)&dev_ancestor,cum_nchild[nodes-1]*sizeof(uint_t)));
	_CUDA(cudaMalloc((void**)&dev_value,x_size*cum_nchild[nodes-1]*sizeof(*dev_value)));
	_CUDA(cudaMalloc((void**)&dev_kgain,x_size*u_size*sizeof(*dev_kgain)));
	_CUDA(cudaMalloc((void**)&dev_u, u_size*nodes*sizeof(*dev_u)));
	_CUDA(cudaMalloc((void**)&dev_matA, x_size*x_size*sizeof(*dev_matA)));
	_CUDA(cudaMalloc((void**)&dev_matB, x_size*u_size*sizeof(*dev_matB)));
	_CUDA(cudaMalloc((void**)&dev_lambda, x_size*nodes*sizeof(*lambda)));

	_CUDA(cudaMemcpy((void**)dev_x,x,x_size*nodes*sizeof(*dev_x),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_cum_child,cum_nchild,nodes*sizeof(int),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_nchild,nchild,nodes*sizeof(int),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_cum_child,cum_nchild,nodes*sizeof(int),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_ancestor,ancestor,cum_nchild[nodes-1]*sizeof(int),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_value,value,x_size*cum_nchild[nodes-1]*sizeof(*dev_value),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_kgain,kgain,x_size*u_size*sizeof(*dev_kgain),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_u,u,nodes*u_size*sizeof(*dev_u),cudaMemcpyHostToDevice));
	_CUBLAS(cublasSetMatrix(x_size,x_size,sizeof(*matA),matA,x_size,dev_matA,x_size));
	_CUBLAS(cublasSetMatrix(x_size,u_size,sizeof(*matB),matB,x_size,dev_matB,x_size));

	cublasHandle_t handle;
	_CUBLAS(cublasCreate(&handle));
	tic();
	control_calculate_test<float>(dev_kgain,dev_x,nodes,dev_u,handle);
	t=toc();
	_CUDA(cudaMemcpy((void**)u,dev_u,nodes*u_size*sizeof(*dev_u),cudaMemcpyDeviceToHost));
	printf("time to calculate control %f \n",t);


	tic();
	system_update_test<float>(dev_x,dev_u,dev_lambda,dev_matA,dev_matB,dev_value,dev_ancestor,nodes,cum_nchild[nodes-1],handle);
	t=toc();
	_CUDA(cudaMemcpy(x,dev_x,x_size*nodes*sizeof(*x)+x_size*cum_nchild[nodes-1]*sizeof(*x),cudaMemcpyDeviceToHost));
	_CUDA(cudaDeviceSynchronize());
	printf("time to update the system %f \n",t);


	FILE * pFile;
	char filename[50];
	sprintf(filename, "back_substitution.h");
	printf("Logging to : '%s'\n", filename);
	pFile = fopen(filename, "w");
	if (pFile == NULL) {
		perror("Error opening file.");
		exit(79);
	}
	for(int i=0;i<nodes+cum_nchild[nodes-1];i++){
		fprintf(pFile, "%d, %f ",i,x[i*x_size]);
		//printf("%d %f ",i,x[i*x_size]);
	}
	printf("\n");
	for(int i=0;i<nodes;i++){
		fprintf(pFile, "%d, %f ",i,u[i*x_size]);
		//printf("%d %f ",i,u[i*x_size]);
	}
	fclose(pFile);
	cublasDestroy(handle);
	//free the memory

	free(x);
	free(u);
	free(value);
	free(lambda);
	free(matA);
	free(kgain);
	free(matB);

	_CUDA(cudaFree(dev_x));
	_CUDA(cudaFree(dev_u));
	_CUDA(cudaFree(dev_value));
	_CUDA(cudaFree(dev_cum_child));
	_CUDA(cudaFree(dev_nchild));
	_CUDA(cudaFree(dev_ancestor));
	_CUDA(cudaFree(dev_kgain));
	_CUDA(cudaFree(dev_lambda));
	_CUDA(cudaFree(dev_matA));
	_CUDA(cudaFree(dev_matB));

}

template<typename T>__global__ void summation_child_test(T *x,T *y,T *value,uint_t* ancestor ){
	int bid=blockIdx.x;
	int tid=threadIdx.x;
	int x_strt=ancestor[(gridDim.x-1-bid)]*NX;
	x[(gridDim.x-1-bid)*blockDim.x+tid]=y[x_strt+tid]+value[(gridDim.x-1-bid)*blockDim.x+tid];
}

template<typename T>__global__ void parallel_sum_test(T *a,const T *b,int size){
	int vec_ele=blockIdx.x*blockDim.x+threadIdx.x;
	if(vec_ele<size)
		a[vec_ele]=a[vec_ele]+b[vec_ele];
}

template<typename T>__host__ void system_update_test(T *x,T *u,T *lambda,T *matA,
		T *matB,T *value, uint_t* ancestor,int node,int cum_nchild,cublasHandle_t handle){
	//cublasHandle_t handle;
	//cublasStatus_t stat;
	//stat=cublasCreate(&handle);

	float al=1.0;
	float bet=0.0;
	_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N,CUBLAS_OP_N,NX_t,node,NX_t,&al,matA,NX_t,x,NX_t,&bet,lambda,NX_t));
	_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N,CUBLAS_OP_N,NX_t,node,NU_t,&al,matB,NX_t,u,NU_t,&al,lambda,NX_t));
	summation_child_test<real_t><<<cum_nchild,NX_t>>>(x+node*NX_t,lambda,value,ancestor);
	//free(h_lambda);

	//cublasDestroy(handle);
}
template<typename T>__host__ void control_calculate_test(const T *kgain,const T* x,const int node, T* u,cublasHandle_t handle){

	float al=1.0;
	float bet=1.0;
	_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N,CUBLAS_OP_N,NU_t,node,NX_t,&al,kgain,NU_t,x,NX_t,&bet,u,NU_t));
}
#endif /* FORWARDSUBSTITUTION_CUH_ */
