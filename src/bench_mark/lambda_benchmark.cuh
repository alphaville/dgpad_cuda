#ifndef _LAMBDA_BENCHMARK_
#define _LAMBDA_BENCHMARK_

#include<curand.h>
#include "cuda_timer.cuh"
#define NX_t 10;
#define BlockSize 16;



typedef float real_t;
typedef unsigned int uint_t;
/*
 * Here we define a function that benchmarks the summation of lambda in the dGPAD algorithm
 * First decide the dimension of NX and BlockSize.
 * The nodes and number of children at each node are generated.
 * sum_lambda_test function is the kernel that perform the summation at each node.
 * The result of the summation is written in the file sum_lambda.txt.
 *
 */

void test_summation_rndgenration();

/*
 * sum_lambda_test
 */
template<typename T> __global__ void sum_lambda_test(const T *q, T *lambda,uint_t* nchild,
		uint_t *cum_nchild,uint_t *nnodes);

/* Implementation  */
void test_summation_rndgenration(){
	//host side data
	real_t *h_q=NULL; //q in the host
	real_t *h_lambda=NULL;//lambda in the host

	const uint_t q_max = 30000;
	const uint_t x_size=NX_t;
	const uint_t ntot=q_max*NX_t;
	const uint_t size_tot = sizeof(real_t) * ntot;

	const uint_t no_child_rep=3;
	const uint_t rep_child[no_child_rep]={150,100,100};
	const uint_t no_nodes=240;
	const uint_t bs=BlockSize;
	float t;
	uint_t *nchild;
	uint_t *cum_nchild;
	nchild=(uint_t*)malloc(no_nodes*sizeof(uint_t));
	cum_nchild=(uint_t*)malloc(no_nodes*sizeof(uint_t));
	h_q=(float*)malloc(size_tot);
	h_lambda=(float*)malloc(size_tot);

	int j=0;
	for(int i=0;i<no_nodes;i++){
		nchild[i]=rep_child[j];
		if(j==no_child_rep-1)
			j=0;
		else
			j=j+1;

		if(i==0){
			cum_nchild[i]=nchild[i];
			for(int m=0;m<nchild[i];m++)
				for(int k=0;k<x_size;k++){
					h_q[m*x_size+k]=1;
					h_lambda[m*x_size+k]=1;
				}
		}
		else{
			cum_nchild[i]=cum_nchild[i-1]+nchild[i];
			for(int m=0;m<nchild[i];m++)
				for(int k=0;k<x_size;k++){
					h_q[(m+cum_nchild[i-1])*x_size+k]=1;
					h_lambda[(m+cum_nchild[i-1])*x_size+k]=2;
				}
		}
	}

	start_tictoc();
	//device side date;
	real_t *dev_q = NULL; // q in the device.
	real_t *dev_lambda = NULL;  //lambda in the device
	uint_t *dev_nchild=NULL;
	uint_t *dev_cum_nchild=NULL;
	uint_t *dev_nnodes=NULL;

	_CUDA(cudaMalloc((void**)&dev_q, size_tot));
	_CUDA(cudaMalloc((void**)&dev_lambda, size_tot));
	_CUDA(cudaMalloc((void**)&dev_nchild,no_nodes*sizeof(uint_t)));
	_CUDA(cudaMalloc((void**)&dev_cum_nchild,no_nodes*sizeof(uint_t)));
	_CUDA(cudaMalloc((void**)&dev_nnodes,sizeof(uint_t)));

	_CUDA(cudaMemcpy((void**)dev_q,h_q,cum_nchild[no_nodes-1]*x_size*sizeof(*dev_q),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_lambda,h_lambda,cum_nchild[no_nodes-1]*x_size*sizeof(*dev_lambda),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_nchild,nchild,no_nodes*sizeof(uint_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_cum_nchild,cum_nchild,no_nodes*sizeof(uint_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy((void**)dev_nnodes,&no_nodes,sizeof(uint_t),cudaMemcpyHostToDevice));


	tic();
	sum_lambda_test<real_t> <<<no_nodes,bs>>>(dev_q,dev_lambda,dev_nchild,dev_cum_nchild,dev_nnodes);
	_CUDA(cudaDeviceSynchronize());
	t = toc();
	printf("RNG in %f ms\n", t);
	checkCudaErrors(cudaMemcpy(h_lambda,dev_lambda,no_nodes*x_size*sizeof(*dev_lambda),cudaMemcpyDeviceToHost));
	FILE * pFile;
	char filename[50];
	sprintf(filename, "sum_lambda.txt");
	printf("Logging to : '%s'\n", filename);
	pFile = fopen(filename, "w");
	if (pFile == NULL) {
		perror("Error opening file.");
		exit(79);
	}
	for(int i=0;i<no_nodes;i++){
		fprintf(pFile, "%d, %f \n",i,h_lambda[i*x_size]);
		//printf("%d %f ",i,h_lambda[i*x_size]);
	}
	fclose(pFile);

	_CUDA(cudaFree(dev_q));
	_CUDA(cudaFree(dev_lambda));
	_CUDA(cudaFree(dev_nchild));
	_CUDA(cudaFree(dev_cum_nchild));
	_CUDA(cudaFree(dev_nnodes));

	free(h_q);
	free(h_lambda);
	free(nchild);
	free(cum_nchild);

}

template<typename T> __global__ void sum_lambda_test(const T *q, T *lambda,uint_t * nchild,
		uint_t *cum_nchild,uint_t *nnodes){

	uint_t x_size=NX_t;
	uint_t q_start_idx;
	uint_t bid=blockIdx.x;
	uint_t tid=threadIdx.x;
	if(tid<x_size){
	if(bid==0)
		q_start_idx=tid;
	else
		q_start_idx=cum_nchild[bid-1]*x_size+tid;

	uint_t nchild_node=nchild[bid];
	uint_t lambda_strt = bid*x_size+tid;

		//__threadfence_block();
		for (int i = 0; i < nchild_node - 1; i++) {
			if (i >0)
				lambda[lambda_strt] =
						lambda[lambda_strt]+ q[q_start_idx + (i + 1) * x_size];
			else
				lambda[lambda_strt] = q[q_start_idx+i *x_size]+ q[q_start_idx + (i + 1) * x_size];
		}
	}
}
#endif
