/*
 *  New_system_header.h
 *  Created on: Nov 1, 2014
 *      Author: Pantelis Sopasakis
 */

#ifndef NEW_SYSTEM_HEADER_H_
#define NEW_SYSTEM_HEADER_H_


#include "../gpadsys_simpleheader.h"
real_t *FN;
real_t *gN;
real_t *GPAD_K_GAIN;
real_t *GPAD_PHI;
real_t *GPAD_THETA;
real_t *GPAD_D;
real_t *GPAD_H;
real_t *GPAD_F;
void create_header_data();
template<typename T>void allocate_data(string  filename,T * data);
//#include "StreamArrayReader.cuh"

#endif /* NEW_SYSTEM_HEADER_H_ */
