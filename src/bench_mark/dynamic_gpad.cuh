/*
 * dynamic_gpad.cuh
 *
 *  Created on: Oct 31, 2014
 *      Author: ajay
 */

#ifndef DYNAMIC_GPAD_CUH_
#define DYNAMIC_GPAD_CUH_


#include<curand.h>
#include"cublas_v2.h"
#include "cuda_timer.cuh"
#include<assert.h>
#include "../gpad_util.cuh"
/**
 *  Function inital_dynamic_gpad_test();
 *  This function initialize the gpad algorithm. The data from the sys_data is transferred to the processor.
 */
__host__ void dynamic_gpad_test();

/* Implementation*/
__host__ void dynamic_gpad_test(){
	real_t alpha;
	init_GPAD_Batch();

	//init_GPAD();
	//device  variables
	real_t* y=(real_t*)malloc(2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t));//dual variables (v,v+1)
	real_t* w=(real_t*)malloc((NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)); //accelerated dual variable
	real_t* x=(real_t*)malloc(NX*N_NODES*sizeof(real_t));
	real_t* u=(real_t*)malloc(NU*N_NONLEAF_NODES*sizeof(real_t));

	_CUDA(cudaMalloc((void**)&dev_y,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_w,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_x,NX*N_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_u,NU*N_NONLEAF_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_q,GN_NUMEL*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_lambda,GN_NUMEL*sizeof(real_t)));

	_CUDA(cudaMemcpy(dev_y,dgpad_y_test1,NC*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_y+NC*N_NONLEAF_NODES,dgpad_yt_test1,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_y+(NC*N_NONLEAF_NODES+GN_NUMEL),dgpad_y_test2,
			(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_y+(2*NC*N_NONLEAF_NODES+GN_NUMEL),dgpad_yt_test2,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));
    _CUDA(cudaMemcpy(dev_w,dgpad_w_test,NC*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_w+NC*N_NONLEAF_NODES,dgpad_wt_test,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));
	/*_CUDA(cudaMemcpy(dev_x,dgpad_x0_test,NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_q,dgpad_wt_test,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_lambda,dgpad_wt_test,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemset(dev_u,0,N_NONLEAF_NODES*NU*sizeof(real_t)));

	/**Device pointers*/
	/*real_t** ptr_w=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_x=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_u=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t** ptr_s=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t** ptr_q=(real_t**)malloc(K*sizeof(real_t*));
	real_t** ptr_lambda=(real_t**)malloc(K*sizeof(real_t*));

	for(int i=0;i<N_NODES;i++){
		ptr_x[i]=&dev_x[i*NX];
		if(i<N_NONLEAF_NODES){
			ptr_w[i]=&dev_w[i*NC];
			ptr_u[i]=&dev_u[i*NU];
		}else{
			ptr_w[i]=&dev_w[N_NONLEAF_NODES*NC+FN_ROWS_CUMUL[i-N_NONLEAF_NODES]];
			ptr_q[i-N_NONLEAF_NODES]=&dev_q[(i-N_NONLEAF_NODES)*NX];
			ptr_lambda[i-N_NONLEAF_NODES]=&dev_lambda[(i-N_NONLEAF_NODES)*NX];
		}
	}
	_CUDA(cudaMalloc((void**)&ptr_dev_x,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_w,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_u,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_s,N_NONLEAF_NODES*(sizeof(real_t*))));
	_CUDA(cudaMalloc((void**)&ptr_dev_q,K*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_lambda,K*sizeof(real_t*)));

	_CUDA(cudaMemcpy(ptr_dev_x,ptr_x,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_w,ptr_w,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_u,ptr_u,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_s,ptr_u,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_q,ptr_q,K*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_lambda,ptr_lambda,K*sizeof(real_t*),cudaMemcpyHostToDevice));

	for(int i=0;i<2;i++)
		ntheta[i]=dgpad_theta_test[i];
	printf("%f %f ",ntheta[0],ntheta[1]);
	alpha=ntheta[1]*(1/ntheta[0]-1);
	printf("%f \n",alpha);
	/*_CUDA(cudaMemset(dev_w,0,(N_NONLEAF_NODES*NC+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMemcpy(w,dev_w,(N_NONLEAF_NODES*NC+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));
	for(int i=0;i<(N_NONLEAF_NODES*NC+GN_NUMEL);i++){
		if(i<N_NONLEAF_NODES*NC)
			printf("%d %f ",i,w[i]);
		else
			printf("%d %f ",i,w[i]);
	}
	printf("\n");*/
	/*cublasHandle_t handle;
	_CUBLAS(cublasCreate(&handle));
	start_tictoc();
	real_t t;
	tic();
	accelerated_dual_update<real_t><<<(N_NONLEAF_NODES+GN_NUMEL),NC>>>(dev_y,&dev_y[N_NONLEAF_NODES*NC+GN_NUMEL],dev_w,alpha);
	dynamic_GPAD_Batch<real_t>(ptr_dev_w,ptr_dev_q,ptr_dev_lambda,ptr_dev_s,ptr_dev_x,ptr_dev_u,handle);
	dual_update(dev_w,dev_x,dev_u,dev_y,handle);
	//dynamic_GPAD<real_t>(dev_y,dev_q,dev_lambda,dev_x,dev_u,handle);
	t=toc();
	printf("time is %f \n",t);
	real_t tolerance=10e-1;
	_CUDA(cudaMemcpy(w,dev_w,(N_NONLEAF_NODES*NC+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));
	for(int i=0;i<(N_NONLEAF_NODES*NC+GN_NUMEL);i++){
		if(i<N_NONLEAF_NODES*NC){
			//printf("%d %f %f ",i,dgpad_w_test[i],w[i]);
			assert(fabs(dgpad_w_test[i]-w[i])<tolerance*1e-2);
		}else{
			//printf("%d %f %f ",i,dgpad_wt_test[i-N_NONLEAF_NODES*NC],w[i]);
			assert(fabs(dgpad_wt_test[i-N_NONLEAF_NODES*NC]-w[i])<tolerance*1e-2);
		}
	}
	_CUDA(cudaMemcpy(w,dev_y+(N_NONLEAF_NODES*NC+GN_NUMEL),(N_NONLEAF_NODES*NC+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));
	for(int i=0;i<(N_NONLEAF_NODES*NC+GN_NUMEL);i++){
		if(i<N_NONLEAF_NODES*NC)
			assert(fabs(dgpad_y_test1[i]-w[i])<tolerance*1e-2);
		else
			assert(fabs(dgpad_yt_test1[i-N_NONLEAF_NODES*NC]-w[i])<tolerance*1e-2);
	}
	printf("Acceleration update of the dual vector is correct \n");

	_CUDA(cudaMemcpy(u,dev_u,NU*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyDeviceToHost));
	_CUDA(cudaMemcpy(x,dev_x,NX*N_NODES*sizeof(real_t),cudaMemcpyDeviceToHost));

	for(int i=0;i<N_NONLEAF_NODES*NU;i++){
		//printf("%d %f ",i,u[i*NU]-dgpad_z_utest[i*NU]);
		if(dgpad_z_utest[i]!=0){
			assert(fabs((u[i]-dgpad_z_utest[i])*100/dgpad_z_utest[i])<tolerance);
		}
	}
	printf("control is less than tolerance \n");
	for(int i=0;i<N_NODES*NX;i++){
		//printf("%d %f ",i,x[i*NX]-dgpad_z_xtest[i*NX]);
		if(dgpad_z_xtest[i]!=0){
			assert(fabs((x[i]-dgpad_z_xtest[i])/dgpad_z_xtest[i])<tolerance);
		}
	}
	printf("states are less than tolerance \n");

	_CUDA(cudaMemcpy(w,dev_y,(N_NONLEAF_NODES*NC+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));
	for(int i=0;i<(N_NONLEAF_NODES*NC+GN_NUMEL);i++){
		if(i<N_NONLEAF_NODES*NC){
			//printf("%d %f ",i,dgpad_yres_test[i]-w[i]);
			assert(fabs((dgpad_yres_test[i]-w[i]))<tolerance*1e-2);
		}else{
			//printf("%d %f ",i,dgpad_ytres_test[i-N_NONLEAF_NODES*NC]-w[i]);
			assert(fabs(dgpad_ytres_test[i-N_NONLEAF_NODES*NC]-w[i])<tolerance*1e-2);
		}
	}
	printf(" dual vector update is correct \n");
	free(y);
	free(x);
	free(u);
	free(ptr_x);
	free(ptr_w);
	free(ptr_u);
	free(ptr_q);
	free(ptr_lambda);

	_CUDA(cudaFree(dev_q));
	_CUDA(cudaFree(dev_lambda));
	_CUDA(cudaFree(dev_x));
	_CUDA(cudaFree(dev_y));
	_CUDA(cudaFree(dev_w));
	_CUDA(cudaFree(dev_u));

	_CUDA(cudaFree(ptr_dev_q));
	_CUDA(cudaFree(ptr_dev_lambda));
	_CUDA(cudaFree(ptr_dev_x));
	_CUDA(cudaFree(ptr_dev_w));
	_CUDA(cudaFree(ptr_dev_u));

	cublasDestroy(handle);
	free_GPAD_Batch();*/
	//free_GPAD();

	printf("dynamic gpad testing");
}

#endif /* DYNAMIC_GPAD_CUH_ */
