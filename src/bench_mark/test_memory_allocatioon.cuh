/*
 * test_memory_allocatioon.cuh
 *
 *  Created on: Nov 22, 2014
 *      Author: ajay
 */
#include "../api/gpad_util_api.cuh"
#include"cublas_v2.h"
#include "error_handles.cuh"
#include "cuda_timer.cuh"
#ifndef TEST_MEMORY_ALLOCATIOON_CUH_
#define TEST_MEMORY_ALLOCATIOON_CUH_

int test_init_GPAD_Batch();
int test_free_GPAD_Batch();
template<typename T> void check_data(T* x,T* y,int n);
void check_memory_allocation();

int test_init_GPAD_Batch(){
	//init_const_mem();

	_CUDA(cudaMalloc((void**)&dev_A,NX*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_B,NU*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_F,NC*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_G,NC*NU*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_g,NC*N_NONLEAF_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_FN,GN_NUMEL*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_GN,GN_NUMEL*sizeof(real_t)));

	/* Device variables - declarations */
	const int n_cuda_vars = 8;

	/* Construct the streams */
	cudaStream_t* copy_streams = (cudaStream_t*) malloc(
			n_cuda_vars * sizeof(cudaStream_t)); // Creation of data streams
	if (copy_streams == NULL) {
		fprintf(stderr, "Allocation error at line %d of %s \n", __LINE__,
				__FILE__);
		exit(500);
	}
	for (int i = 0; i < n_cuda_vars; i++) {
		checkCudaErrors(cudaStreamCreate(&(copy_streams[i])));
	}

	/* Memory allocation on the host [cudaMallocHost] 	*/
	/* as write-combined memory 						*/
	//cudaHostAllocWriteCombined
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_K_GAIN,
					DIM_GPAD_K_GAIN * sizeof(*dev_GPAD_K_GAIN)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_SIGMA,
					N_NONLEAF_NODES * NU * sizeof(*dev_GPAD_SIGMA)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_PHI,
					N_NONLEAF_NODES * NC * NU
					* sizeof(*dev_GPAD_PHI)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_THETA,
					((N_NONLEAF_NODES-K)*NX*NU+(GN_NUMEL)*NU) * sizeof(*dev_GPAD_THETA)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_C,
					N_NONLEAF_NODES * NX * sizeof(*dev_GPAD_C)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_D,
					N_NONLEAF_NODES * NC * NX * sizeof(*dev_GPAD_D)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_H,
					N_NONLEAF_NODES * NU * NX * sizeof(*dev_GPAD_H)));
	checkCudaErrors(
			cudaMallocHost((void**) &dev_GPAD_F,
					((N_NONLEAF_NODES-K)*NX*NX+(GN_NUMEL)*NX)* sizeof(*dev_GPAD_F)));

	/* Copy data onto the device [Asynchronously] */
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_K_GAIN, GPAD_K_GAIN,
					DIM_GPAD_K_GAIN * sizeof(*dev_GPAD_K_GAIN),
					cudaMemcpyHostToDevice, copy_streams[0]));
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_SIGMA, GPAD_SIGMA,
					N_NONLEAF_NODES * NU * sizeof(*dev_GPAD_SIGMA),
					cudaMemcpyHostToDevice, copy_streams[1]));
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_PHI, GPAD_PHI,
					(N_NONLEAF_NODES) * NC * NU
					* sizeof(real_t), cudaMemcpyHostToDevice,
					copy_streams[2]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_THETA, GPAD_THETA,
					((N_NONLEAF_NODES-K)*NX*NU+(GN_NUMEL)*NU)  * sizeof(*dev_GPAD_THETA),
					cudaMemcpyHostToDevice, copy_streams[3]));
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_C, GPAD_C,
					N_NONLEAF_NODES * NX * sizeof(*dev_GPAD_C),
					cudaMemcpyHostToDevice, copy_streams[4]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_D, GPAD_D,
					N_NONLEAF_NODES * NC * NX * sizeof(*dev_GPAD_D),
					cudaMemcpyHostToDevice, copy_streams[5]));
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_H, GPAD_H,
					N_NONLEAF_NODES * NU * NX * sizeof(*dev_GPAD_H),
					cudaMemcpyHostToDevice, copy_streams[6]));
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_F, GPAD_F,
					((N_NONLEAF_NODES-K)*NX*NX+(GN_NUMEL)*NX) * sizeof(*dev_GPAD_F),
					cudaMemcpyHostToDevice, copy_streams[7]));
	/* Synchronize [and check for errors] */
	checkCudaErrors(cudaDeviceSynchronize());
	//printf("SUCCESSFULL cpoy of memory OF gpad_d \n");
	/* Destroy the copy streams */
	for (int i = 0; i < n_cuda_vars; i++) {
		checkCudaErrors(cudaStreamDestroy(copy_streams[i]));
	}
	/* Free memory for copy_streams */
	if (copy_streams)
		free(copy_streams);

	_CUDA(cudaMemcpy(dev_A,A,NX*NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_B,B,NX*NU*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_F,F,NC*NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_G,G,NC*NU*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_g,g,NC*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_FN,FN,GN_NUMEL*NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_GN,gN,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));


	/** Host pointers*/
	real_t **ptr_GPAD_K_GAIN=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_SIGMA=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_PHI=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_THETA=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_C=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_D=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_H=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_F=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));

	/** Device pointers */
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_K_GAIN,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_SIGMA,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_PHI,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_THETA,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_C,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_D,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_H,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_F,N_NONLEAF_NODES*sizeof(real_t*)));

	for(int i=0;i<N_NONLEAF_NODES;i++){
		ptr_GPAD_K_GAIN[i]=&dev_GPAD_K_GAIN[i*NX*NU];
		ptr_GPAD_SIGMA[i]=&dev_GPAD_SIGMA[i*NU];
		ptr_GPAD_PHI[i]=&dev_GPAD_PHI[i*NU*NC];
		ptr_GPAD_C[i]=&dev_GPAD_C[i*NX];
		ptr_GPAD_D[i]=&dev_GPAD_D[i*NX*NC];
		ptr_GPAD_H[i]=&dev_GPAD_H[i*NX*NU];
		if(i<N_NONLEAF_NODES-K){
			ptr_GPAD_F[i]=&dev_GPAD_F[i*NX*NX];
			ptr_GPAD_THETA[i]=&dev_GPAD_THETA[i*NU*NX];
		}
		else{
			ptr_GPAD_THETA[i]=&dev_GPAD_THETA[(N_NONLEAF_NODES-K)*NU*NX+FN_ROWS_CUMUL[i-(N_NONLEAF_NODES-K)]*NU];
			ptr_GPAD_F[i]=&dev_GPAD_F[(N_NONLEAF_NODES-K)*NX*NX+FN_ROWS_CUMUL[i-(N_NONLEAF_NODES-K)]*NX];
		}
	}


	_CUDA(cudaMemcpy(ptr_dev_GPAD_K_GAIN,ptr_GPAD_K_GAIN,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_SIGMA,ptr_GPAD_SIGMA,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_PHI,ptr_GPAD_PHI,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_THETA,ptr_GPAD_THETA,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_C,ptr_GPAD_C,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_D,ptr_GPAD_D,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_H,ptr_GPAD_H,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_F,ptr_GPAD_F,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));


	free(ptr_GPAD_K_GAIN);
	free(ptr_GPAD_SIGMA);
	free(ptr_GPAD_PHI);
	free(ptr_GPAD_THETA);
	free(ptr_GPAD_C);
	free(ptr_GPAD_D);
	free(ptr_GPAD_H);
	free(ptr_GPAD_F);
	return 0;
}

int test_free_GPAD_Batch(){
	if (dev_GPAD_C)
		checkCudaErrors(cudaFreeHost(dev_GPAD_C));
	if (dev_GPAD_D)
		checkCudaErrors(cudaFreeHost(dev_GPAD_D));
	if (dev_GPAD_F)
		checkCudaErrors(cudaFreeHost(dev_GPAD_F));
	if (dev_GPAD_H)
		checkCudaErrors(cudaFreeHost(dev_GPAD_H));
	if (dev_GPAD_K_GAIN)
		checkCudaErrors(cudaFreeHost(dev_GPAD_K_GAIN));
	if (dev_GPAD_PHI)
		checkCudaErrors(cudaFreeHost(dev_GPAD_PHI));
	if (dev_GPAD_SIGMA)
		checkCudaErrors(cudaFreeHost(dev_GPAD_SIGMA));
	if (dev_GPAD_THETA)
		checkCudaErrors(cudaFreeHost(dev_GPAD_THETA));
	if(dev_A)
		_CUDA(cudaFree(dev_A));
	if(dev_B)
		_CUDA(cudaFree(dev_B));
	if(dev_F)
		_CUDA(cudaFree(dev_F));
	if(dev_G)
		_CUDA(cudaFree(dev_G));
	if(dev_FN)
		_CUDA(cudaFree(dev_FN));
	if(dev_g)
		_CUDA(cudaFree(dev_g));
	if(dev_GN)
		_CUDA(cudaFree(dev_GN));
	if (ptr_dev_GPAD_C)
		_CUDA(cudaFree(ptr_dev_GPAD_C));
	if (ptr_dev_GPAD_D)
		_CUDA(cudaFree(ptr_dev_GPAD_D));
	if (ptr_dev_GPAD_F)
		_CUDA(cudaFree(ptr_dev_GPAD_F));
	if (ptr_dev_GPAD_H)
		_CUDA(cudaFree(ptr_dev_GPAD_H));
	if (ptr_dev_GPAD_K_GAIN)
		_CUDA(cudaFree(ptr_dev_GPAD_K_GAIN));
	if (ptr_dev_GPAD_PHI)
		_CUDA(cudaFree(ptr_dev_GPAD_PHI));
	if (ptr_dev_GPAD_SIGMA)
		_CUDA(cudaFree(ptr_dev_GPAD_SIGMA));
	if (ptr_dev_GPAD_THETA)
		_CUDA(cudaFree(ptr_dev_GPAD_THETA));

	return 0;
}

template<typename T> void check_data(T* x,T* y,int n){
	T* h_x=(T*)malloc(n*sizeof(T));
	_CUDA(cudaMemcpy(h_x,x,n*sizeof(real_t),cudaMemcpyDeviceToHost));
	check_correctness_memcpy(h_x,y,n);
	free(h_x);
}
void check_memory_allocation(){
	printf("BEGIN ALLOCATION OF MEMORY \n");
	test_init_GPAD_Batch();
	printf("SUCCESSFULL ALLOCATION OF MEMORY \n");
	check_data<real_t>(dev_GPAD_THETA,temp_GPAD_THETA,((N_NONLEAF_NODES-K)*NX*NU+(GN_NUMEL)*NU));
	check_data<real_t>(dev_GPAD_K_GAIN,temp_GPAD_K_GAIN,DIM_GPAD_K_GAIN);
	check_data<real_t>(dev_GPAD_D,temp_GPAD_D,N_NONLEAF_NODES * NC * NX);
	check_data<real_t>(dev_GPAD_H,temp_GPAD_H,N_NONLEAF_NODES * NU * NX);
	check_data<real_t>(dev_GPAD_K_GAIN,temp_GPAD_K_GAIN,DIM_GPAD_K_GAIN);
	check_data<real_t>(dev_GN,temp_gN,GN_NUMEL);
	check_data<real_t>(dev_FN,temp_FN,FN_NUMEL);
	check_data<real_t>(dev_GPAD_F,temp_GPAD_F,((N_NONLEAF_NODES-K)*NX*NX+(GN_NUMEL)*NX));
	check_data<real_t>(dev_GPAD_PHI,temp_GPAD_PHI,N_NONLEAF_NODES * NC * NU);
	test_free_GPAD_Batch();
	printf("SUCCESSFULL DEALLOCATION OF MEMORY \n");
}
#endif /* TEST_MEMORY_ALLOCATIOON_CUH_ */
