/*
 * StreamArrayReader.cuh
 *
 *  Created on: Nov 1, 2014
 *      Author: Pantelis Sopasakis
 */

#ifndef STREAMARRAYREADER_IMPL_
#define STREAMARRAYREADER_IMPL_

#include "../gpadsys_simpleheader.h"
#include<string.h>
#include<stddef.h>
#include<stdio.h>
#include<stdlib.h>
typedef float real_t;

real_t *FN;
real_t *gN;
real_t *GPAD_K_GAIN;
real_t *GPAD_PHI;
real_t *GPAD_THETA;
real_t *GPAD_D;
real_t *GPAD_H;
real_t *GPAD_F;
void create_header_data();
template<typename T>void allocate_data(char*  filename,T* data);


template<typename T> void allocate_data(char* filepath, T* data){
	FILE *infile;
	int size;
	infile=fopen(filepath,"r");
	if(infile==NULL){
		fprintf(stderr,"Error in opening the file",__LINE__);
		exit(100);
	}else{
		fscanf(infile,"%d \n",&size);
		printf("Size of the array is %d ",size);
		data=(real_t*)malloc(size*sizeof(T));
		for(int i=0;i<size;i++){
			fscanf(infile,"%f\n",&data[i]);
		}
	}
}


void create_header_data(){
	char* filepath_FN = "Data_files/GPAD_FN.h";
	char* filepath_gN=	"Data_files/GPAD_gN.h";
	char* filepath_K_GAIN= "Data_files/GPAD_K_GAIN.h";
	char* filepath_PHI = "Data_files/GPAD_PHI.h";
	//string filepath_PHI = "Data_files/GPAD_THETA.h";
	char* filepath_THETA = "Data_files/GPAD_THETA.h";
	char* filepath_D = "Data_files/GPAD_D.h";
	char* filepath_H = "Data_files/GPAD_H.h";
	char* filepath_F = "Data_files/GPAD_F.h";

	allocate_data(filepath_FN,FN);
	printf("SUCCESSFULL ALLOCATION OF FN MEMORY \n");
	allocate_data(filepath_K_GAIN,	GPAD_K_GAIN);
	printf("SUCCESSFULL ALLOCATION OF GPAD_K MEMORY \n");
	allocate_data(filepath_D,GPAD_D);
	printf("SUCCESSFULL ALLOCATION OF D MEMORY \n");
	allocate_data(filepath_H,GPAD_H);
	printf("SUCCESSFULL ALLOCATION OF H MEMORY \n");
	allocate_data(filepath_THETA,GPAD_THETA);
	printf("SUCCESSFULL ALLOCATION OF THETA MEMORY \n");
	allocate_data(filepath_F,FN);
	printf("SUCCESSFULL ALLOCATION OF F MEMORY \n");
	allocate_data(filepath_gN,gN);
	printf("SUCCESSFULL ALLOCATION OF gN MEMORY \n");
	allocate_data(filepath_PHI,	GPAD_PHI);
	printf("SUCCESSFULL ALLOCATION OF PHI MEMORY \n");
	//StreamArrayReader<real_t> sar_FN((char *)filepath_FN.c_str());
	//FN = sar_FN.getData();
	//StreamArrayReader<real_t> sar_GPAD_K((char *)filepath_K_GAIN.c_str());
	//GPAD_K_GAIN = sar_GPAD_K.getData();
	/*StreamArrayReader<real_t> sar_D((char *)filepath_D.c_str());
	GPAD_D = sar_D.getData();

	StreamArrayReader<real_t> sar_H((char *)filepath_H.c_str());
	GPAD_H = sar_H.getData();

	StreamArrayReader<real_t> sar_THETA((char *)filepath_THETA.c_str());
	GPAD_THETA = sar_THETA.getData();

	StreamArrayReader<real_t> sar_F((char *)filepath_F.c_str());
	GPAD_F = sar_F.getData();

	StreamArrayReader<real_t> sar_gN((char *)filepath_gN.c_str());
	gN = sar_gN.getData();

	StreamArrayReader<real_t> sar_PHI((char *)filepath_PHI.c_str());
	GPAD_PHI = sar_PHI.getData();

	//filepath = "Data_files/GPAD_D.h";

	//filepath = "Data_files/GPAD_H.h";

	//filepath = "Data_files/GPAD_K_GAIN.h";

	//filepath = "Data_files/GPAD_PHI.h";

	//filepath = "Data_files/GPAD_THETA.h";*/
	printf("OK!\n");
}
/*template<typename T>void allocate_data(string  filepath,T * data){
	StreamArrayReader<real_t> sar((char *)filepath.c_str());
	data = sar.getData();
}
*/
#endif
