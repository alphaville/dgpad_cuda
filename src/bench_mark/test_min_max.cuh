/*
 * test_min_max.cuh
 *
 *  Created on: Nov 20, 2014
 *      Author: ajay
 */

#ifndef TEST_MIN_MAX_CUH_
#define TEST_MIN_MAX_CUH_

typedef unsigned int uint_t;
typedef float real_t;

#include<curand.h>
#include"cublas_v2.h"
#include "cuda_timer.cuh"
#include<assert.h>
#include "../gpad_util.cuh"

//template<typename T>__global__ void find_max(T *x,T *y,int lenght,int nthreads);
void test_max();

/*template<typename T>__global__ void find_max(T *x,T *y,int lenght,int nthreads){
	int tid=threadIdx.x+blockIdx.x*blockDim.x;
	int Lvector=lenght;
	int Tthreads=nthreads;
	while(Tthreads>0){
		if(tid<Tthreads){
			if(Lvector==lenght){
				if((Lvector%2)==0){
					y[tid]=(x[tid]>x[tid+Tthreads] ? x[tid]:x[tid+Tthreads]);
				}else{
					if(tid==Tthreads-1){
						y[tid]=(x[tid]>x[tid+Tthreads] ? x[tid]:x[tid+Tthreads]);
						y[tid]=(y[tid]>x[tid+Tthreads+1] ? y[tid]:x[tid+Tthreads+1]);
					}else{
						y[tid]=(x[tid]>x[tid+Tthreads] ? x[tid]:x[tid+Tthreads]);
					}
				}
				Lvector=Lvector/2;
				Tthreads=Tthreads/2;
			}else{
				if((Lvector%2)==0){
					y[tid]=(y[tid]>y[tid+Tthreads] ? y[tid]:y[tid+Tthreads]);
				}else{
					if(tid==Tthreads-1){
						y[tid]=(y[tid]>y[tid+Tthreads] ? y[tid]:y[tid+Tthreads]);
						y[tid]=(y[tid]>y[tid+Tthreads+1] ? y[tid]:y[tid+Tthreads+1]);
					}else{
						y[tid]=(y[tid]>y[tid+Tthreads] ? y[tid]:y[tid+Tthreads]);
					}
				}
				Lvector=Lvector/2;
				Tthreads=Tthreads/2;
			}
		}else{
			Tthreads=0;
		}
		__threadfence();
	}
}*/


void test_max(){
	real_t t;
	int size=GN_NUMEL;
	int res_size=size/2;

	real_t *x=(real_t*)malloc(size*sizeof(real_t));
	real_t *result=(real_t*)malloc(size*sizeof(real_t));

	for(int i=0;i<size;i++){
		x[i]=gN[i];
		//x[i]=-3*(i+2)+(i+2)*(i+6);
	}

	//printf("\n");
	real_t *dev_m;
	real_t *dev_result;


	_CUDA(cudaMalloc((void** )&dev_m, size*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_result,res_size*sizeof(real_t)));
	_CUDA(cudaMemcpy(dev_m,x,size*sizeof(real_t),cudaMemcpyHostToDevice));
	/*curandGenerator_t gen;

	_CURAND(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
	_CURAND(curandSetPseudoRandomGeneratorSeed(gen, 13534ULL));
	_CURAND(curandGenerateUniform(gen, dev_m, size));
	_CURAND(curandDestroyGenerator(gen));*/

	start_tictoc();
	int j;
	for(int k=size;k<size+1;k=k+400){
		tic();
		j=k/2;
		find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NX*NX>>>(dev_m,dev_result,k,j);
		_CUDA(cudaMemcpy(result,dev_result,j*sizeof(real_t),cudaMemcpyDeviceToHost));
		t=toc();
		_CUDA(cudaMemcpy(x,dev_m,k*sizeof(real_t),cudaMemcpyDeviceToHost));
		printf("time to find max is %f %f \n",t,result[0]);
		for(int i=0;i<k;i++){
			if(result[0]<x[i]){
				printf("error %d %d %f %f \n",i,k,x[i],result[0]);
				i=k;
			}
		}
	}
	printf("SUCCESS \n");
	free(x);
	free(result);
	_CUDA(cudaFree(dev_m));
	_CUDA(cudaFree(dev_result));
}




#endif /* TEST_MIN_MAX_CUH_ */
