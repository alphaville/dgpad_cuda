/*
 * gpadsys_header.cuh
 *
 *  Created on: Nov 25, 2014
 *      Author: ajay
 */

#ifndef GPADSYS_HEADER_CUH_
#define GPADSYS_HEADER_CUH_

#include "gpadsys_simpleheader.h"
//#include "temp_gpadsys_data.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
typedef float real_t;

real_t *FN;
real_t *gN;
real_t *GPAD_K_GAIN;
real_t *GPAD_PHI;
real_t *GPAD_THETA;
real_t *GPAD_D;
real_t *GPAD_H;
real_t *GPAD_F;
real_t *F;
real_t *G;
real_t *g;
real_t *V_Vf;
void create_header_data();
void free_host_mem();
template<typename T>int allocate_data(char*  filepath,T* data);

template<typename T> int allocate_data(char* filepath, T* data){
	FILE *infile;
	int size;
	infile=fopen(filepath,"r");
	if(infile==NULL){
		printf("%s\n %p", filepath,infile);
		fprintf(stderr,"Error in opening the file %d \n",__LINE__);
		exit(100);
	}else{
		fscanf(infile,"%d \n",&size);
		//printf("Size of the array is %d ",size);
		for(int i=0;i<size;i++){
			fscanf(infile,"%f\n",&data[i]);
		}
		return 0;
	}
}


template<typename T>void check_correctness_memcpy(T* x,T *y,int n){
	for(int i=0;i<n;i++){
		//printf("%d %f ",i, x[i]-y[i]);
		if(fabs(x[i]-y[i])>1e-3){
			printf("%d ",i);
		}
	}
	printf("SUCESS \n");
}
void create_header_data(){
	GPAD_K_GAIN=(real_t*)malloc(DIM_GPAD_K_GAIN*sizeof(real_t));
	GPAD_THETA=(real_t*)malloc( ((N_NONLEAF_NODES-K)*NX*NU+(GN_NUMEL)*NU)*sizeof(real_t));
	GPAD_D=(real_t*)malloc( N_NONLEAF_NODES * NC * NX * sizeof(real_t));
	GPAD_H=(real_t*)malloc( N_NONLEAF_NODES * NU * NX *sizeof(real_t));
	gN=(real_t*)malloc(GN_NUMEL*sizeof(real_t));
	FN=(real_t*)malloc(FN_NUMEL*sizeof(real_t));
	GPAD_PHI=(real_t*)malloc( N_NONLEAF_NODES * NC * NU*sizeof(real_t));
	GPAD_F=(real_t*)malloc( ((N_NONLEAF_NODES-K)*NX*NX+(GN_NUMEL)*NX)*sizeof(real_t));
	F=(real_t*)malloc(N_NONLEAF_NODES*NC*NX*sizeof(real_t));
	G=(real_t*)malloc(N_NONLEAF_NODES*NC*NU*sizeof(real_t));
	g=(real_t*)malloc(N_NONLEAF_NODES*NC*sizeof(real_t));
	V_Vf=(real_t*)malloc(K*NX*NX*sizeof(real_t));

	char* filepath_gN=	"Data_files/GPAD_gN.h";
	char* filepath_K_GAIN= "Data_files/GPAD_K_GAIN.h";
	char* filepath_THETA = "Data_files/GPAD_THETA.h";
	char* filepath_D = "Data_files/GPAD_D.h";
	char* filepath_H = "Data_files/GPAD_H.h";
	char* filepath_PHI = "Data_files/GPAD_PHI.h";
	char* filepath_FN = "Data_files/GPAD_FN.h";
	char* filepath_F = "Data_files/GPAD_F.h";
	char* filepath_Fc="Data_files/GPAD_Fc.h";
	char* filepath_Gc="Data_files/GPAD_Gc.h";
	char* filepath_g="Data_files/GPAD_g.h";
	char* filepath_Vf="Data_files/GPAD_Vf.h";

	allocate_data<real_t>(filepath_D,GPAD_D);
	allocate_data<real_t>(filepath_K_GAIN,GPAD_K_GAIN);
	allocate_data<real_t>(filepath_H,GPAD_H);
	allocate_data<real_t>(filepath_THETA,GPAD_THETA);
	allocate_data<real_t>(filepath_gN,gN);
	allocate_data<real_t>(filepath_PHI,GPAD_PHI);
	allocate_data<real_t>(filepath_FN,FN);
	allocate_data<real_t>(filepath_F,GPAD_F);
	allocate_data<real_t>(filepath_Fc,F);
	allocate_data<real_t>(filepath_Gc,G);
	allocate_data<real_t>(filepath_g,g);
	allocate_data<real_t>(filepath_Vf,V_Vf);
	printf("OK!\n");
}

void free_host_mem(){
	free(GPAD_K_GAIN);
	free(GPAD_PHI);
	free(GPAD_THETA);
	free(gN);
	free(FN);
	free(GPAD_H);
	free(GPAD_F);
	free(GPAD_D);
	free(F);
	free(G);
	free(g);
	free(V_Vf);
}


#endif /* GPADSYS_HEADER_CUH_ */
