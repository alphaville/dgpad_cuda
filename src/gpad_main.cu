#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include "helper_cuda.h"
#include "gpadsys_header.cuh"
#include "gpad_util.cuh"


int main(void) {

	create_header_data();
	init_GPAD_Batch();
	printf("SUCCESSFULL ALLOCATION OF MEMORY \n");
	//device  variables
	real_t* y=(real_t*)malloc(2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t));//dual variables (v,v+1)
	real_t* primal=(real_t*)malloc(2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t));//primal_infesibility checks (v,v+1)
	real_t* w=(real_t*)malloc(2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)); //accelerated dual variable
	real_t* x=(real_t*)malloc(NX*N_NODES*sizeof(real_t));
	real_t* u=(real_t*)malloc(NU*N_NONLEAF_NODES*sizeof(real_t));
	grad=(real_t*)malloc(iterate[0]*sizeof(real_t));
	prm_cst_value=(real_t*)malloc(iterate[0]*sizeof(real_t));
	dual_cst_value=(real_t*)malloc(iterate[0]*sizeof(real_t));
	prm_inf=(real_t*)malloc(iterate[0]*sizeof(real_t));
	real_t** z=(real_t**)malloc(N_NODES*sizeof(real_t*));

	_CUDA(cudaMalloc((void**)&dev_y,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_primal,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_primal_avg,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_primal_iterate,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_w,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_x,NX*N_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_u,NU*N_NONLEAF_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_q,GN_NUMEL*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_lambda,GN_NUMEL*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_Q,NX*NX*N_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_R,NU*NU*N_NONLEAF_NODES*sizeof(real_t)));

	if(NX*N_NODES>NU*N_NONLEAF_NODES){
		_CUDA(cudaMalloc((void**)&dev_temp_cst,NX*N_NODES*sizeof(real_t)));
	}else{
		_CUDA(cudaMalloc((void**)&dev_temp_cst,NU*N_NONLEAF_NODES*sizeof(real_t)));
	}

	/**Device pointers*/
	real_t** ptr_w=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_y=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_x=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_u=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t** ptr_s=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t** ptr_q=(real_t**)malloc(K*sizeof(real_t*));
	real_t** ptr_lambda=(real_t**)malloc(K*sizeof(real_t*));
	real_t** ptr_lambda_N=(real_t**)malloc(K*sizeof(real_t*));
	real_t** ptr_Q=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_R=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t** ptr_temp_xcst=(real_t**)malloc(N_NODES*sizeof(real_t*));
	real_t** ptr_temp_ucst=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));

	cublasHandle_t handle;
	_CUBLAS(cublasCreate(&handle));


	for(int i=0;i<N_NODES;i++){
		ptr_x[i]=&dev_x[i*NX];
		ptr_Q[i]=&dev_Q[i*NX*NX];
		ptr_temp_xcst[i]=&dev_temp_cst[i*NX];
		if(i<N_NONLEAF_NODES){
			ptr_w[i]=&dev_w[i*NC];
			ptr_y[i]=&dev_y[i*NC];
			ptr_u[i]=&dev_u[i*NU];
			ptr_R[i]=&dev_R[i*NU*NU];
			ptr_temp_ucst[i]=&dev_temp_cst[i*NU];
			_CUDA(cudaMemcpy(&dev_Q[i*NX*NX],Q,NX*NX*sizeof(real_t),cudaMemcpyHostToDevice));
			_CUDA(cudaMemcpy(&dev_R[i*NU*NU],R,NU*NU*sizeof(real_t),cudaMemcpyHostToDevice));
			_CUBLAS(cublasSscal(handle,NU*NU,&TREE_PROB[i],&dev_R[i*NU*NU],1));
		}else{
			ptr_w[i]=&dev_w[N_NONLEAF_NODES*NC+FN_ROWS_CUMUL[i-N_NONLEAF_NODES]];
			ptr_y[i]=&dev_y[N_NONLEAF_NODES*NC+FN_ROWS_CUMUL[i-N_NONLEAF_NODES]];
			ptr_q[i-N_NONLEAF_NODES]=&dev_q[(i-N_NONLEAF_NODES)*NX];
			ptr_lambda[i-N_NONLEAF_NODES]=&dev_lambda[(i-N_NONLEAF_NODES)*NX];
			ptr_lambda_N[i-N_NONLEAF_NODES]=&dev_lambda[FN_ROWS_CUMUL[(i-N_NONLEAF_NODES)]];
			_CUDA(cudaMemcpy(&dev_Q[i*NX*NX],&V_Vf[(i-N_NONLEAF_NODES)*NX*NX],NX*NX*sizeof(real_t),cudaMemcpyHostToDevice));
		}
		_CUBLAS(cublasSscal(handle,NX*NX,&TREE_PROB[i],&dev_Q[i*NX*NX],1));
	}

	_CUDA(cudaMalloc((void**)&ptr_dev_x,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_w,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_y,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_u,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_s,N_NONLEAF_NODES*(sizeof(real_t*))));
	_CUDA(cudaMalloc((void**)&ptr_dev_q,K*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_lambda,K*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_lambda_N,K*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_Q,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_R,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_temp_xcst,N_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_temp_ucst,N_NONLEAF_NODES*sizeof(real_t*)));


	_CUDA(cudaMemcpy(ptr_dev_x,ptr_x,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_w,ptr_w,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_y,ptr_y,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_u,ptr_u,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_s,ptr_u,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_q,ptr_q,K*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_lambda,ptr_lambda,K*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_lambda_N,ptr_lambda_N,K*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_Q,ptr_Q,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_R,ptr_R,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_temp_xcst,ptr_temp_xcst,N_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_temp_ucst,ptr_temp_ucst,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));


	/*_CUDA(cudaMemcpy(z,ptr_dev_Q,N_NODES*sizeof(real_t*),cudaMemcpyDeviceToHost));
	for(int i=0;i<N_NODES;i++){
		_CUDA(cudaMemcpy(prm_cst_value,z[i],NX*NX*sizeof(real_t),cudaMemcpyDeviceToHost));
		for(int ii=0;ii<NX*NX;ii++){
			printf("%f ",prm_cst_value[ii]);
		}
		printf("\n");
	}
	_CUDA(cudaMemcpy(z,ptr_dev_R,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyDeviceToHost));
	for(int i=0;i<N_NONLEAF_NODES;i++){
		_CUDA(cudaMemcpy(prm_cst_value,z[i],NU*NU*sizeof(real_t),cudaMemcpyDeviceToHost));
		//printf("%p",z[0]);
		for(int ii=0;ii<NU*NU;ii++){
			printf("%f ",prm_cst_value[ii]);
		}
		printf("\n");
	}*/

	start_tictoc();
	real_t t;
	printf("Ready to solve GPAD \n");


	FILE* pFile;
	char filename[50];
	sprintf(filename, "results_temp.h");
	printf("Logging to : '%s'\n", filename);
	pFile = fopen(filename, "w");
	if (pFile == NULL) {
		perror("Error opening file.");
		exit(79);
	}

	//test_size
	for(int i=0;i<test_size-1;i++){
		_CUDA(cudaMemset(dev_y,0,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemset(dev_w,0,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemset(dev_primal,0,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemset(dev_primal_avg,0,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemcpy(dev_x,&dgpad_x0_test[i*NX],NX*sizeof(real_t),cudaMemcpyHostToDevice));
		tic();
		GPAD_algorithm<real_t>(dev_x,handle);
		//Mono_GPAD_algorithm<real_t>(dev_x,handle);
		t=toc();
		primal_cost_function<real_t>(handle);
		//printf("%f %f \n",prm_cst[0],prm_cst[1]);

		fprintf(pFile,"Iterates %d ",GPAD_iterates);
		fprintf(pFile,"first condition %d ",GPAD_first_cond_flag);
		fprintf(pFile,"second condition %d ",GPAD_second_cond_flag);
		fprintf(pFile,"termination value %f ",GPAD_primal_inf_term);
		fprintf(pFile,"Time %f \n",t);
		printf("time of computation is %f \n",t);
		fprintf(pFile,"primal cost %f dual cost %f dual gap %f \n", prm_cst[0],dual_cst[0],prm_cst[0]-dual_cst[0]);
		//_CUDA(cudaMemcpy(u,dev_u,NU*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyDeviceToHost));
		/*fprintf(pFile,"{[");
		for(int j=0;j<GPAD_iterates;j++){
			//fprintf(pFile,"%f ",u[j]-dgpad_ures_test[i*NU+j]);
			if(grad[j]<0){
			fprintf(pFile,"%d %f ",j,grad[j]);
			}
		}
		fprintf(pFile,"]\n");
		/*fprintf(pFile,"{[");
		for(int j=0;j<GPAD_iterates;j++){
			fprintf(pFile,"%f ",prm_inf[j]);
		}
		fprintf(pFile,"]\n");
		fprintf(pFile,"[");
		for(int j=0;j<GPAD_iterates;j++){
			if(j>0){
				if(dual_cst_value[j-1]-dual_cst_value[j]>=0){
				fprintf(pFile,"%f ",dual_cst_value[j]);
				}
			}
		}
		fprintf(pFile,"]}\n");*/
		GPAD_iterates=0;
		GPAD_first_cond_flag=0;
		GPAD_second_cond_flag=0;
		GPAD_primal_inf_term=0;
		ntheta[1]=1;
		ntheta[0]=1;


		_CUDA(cudaMemset(dev_y,0,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemset(dev_w,0,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemset(dev_primal,0,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemset(dev_primal_avg,0,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t)));
		_CUDA(cudaMemcpy(dev_x,&dgpad_x0_test[i*NX],NX*sizeof(real_t),cudaMemcpyHostToDevice));
		tic();
		//GPAD_algorithm<real_t>(dev_x,handle);
		Mono_GPAD_algorithm<real_t>(dev_x,handle);
		t=toc();
		primal_cost_function<real_t>(handle);
		//printf("%f %f \n",prm_cst[0],prm_cst[1]);

		fprintf(pFile,"%d Iterates %d ",i,GPAD_iterates);
		fprintf(pFile,"first condition %d ",GPAD_first_cond_flag);
		fprintf(pFile,"second condition %d ",GPAD_second_cond_flag);
		fprintf(pFile,"termination value %f ",GPAD_primal_inf_term);
		fprintf(pFile,"Time %f \n",t);
		printf("time of computation is %f \n",t);
		fprintf(pFile,"primal cost %f dual cost %f dual gap %f \n", prm_cst[0],dual_cst[0],prm_cst[0]-dual_cst[0]);
		//_CUDA(cudaMemcpy(u,dev_u,NU*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyDeviceToHost));
		/*fprintf(pFile,"{[");
		for(int j=0;j<GPAD_iterates;j++){
			if(grad[j]<0){
			fprintf(pFile,"%f ", grad[j]);
			}
		}
		fprintf(pFile,"]\n");
		/*fprintf(pFile,"[");
		for(int j=0;j<GPAD_iterates;j++){
			fprintf(pFile,"%f ",prm_inf[j]);
		}
		fprintf(pFile,"]\n");
		_CUDA(cudaMemcpy(y,dev_primal_iterate,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));
		for(int j=0;j<N_NONLEAF_NODES;j++){
			fprintf(pFile,"[");
			for(int kk=0;kk<NC;kk++){
				fprintf(pFile,"%f ",y[j*NC+kk]);
			}
			fprintf(pFile,"]\n");
		}
		for(int j=0;j<K;j++){
			fprintf(pFile,"[");
			for(int kk=0;kk<16;kk++){
				fprintf(pFile,"%f ",y[N_NONLEAF_NODES*NC+16*j+kk]);
			}
			fprintf(pFile,"]\n");
		}

		fprintf(pFile,"[");
		for(int j=0;j<GPAD_iterates;j++){
			if(j>0){
				if(dual_cst_value[j-1]-dual_cst_value[j]>=0){
				fprintf(pFile,"%f ",dual_cst_value[j]);
				}
			}
		}
		fprintf(pFile,"]}\n");*/
		GPAD_iterates=0;
		GPAD_first_cond_flag=0;
		GPAD_second_cond_flag=0;
		GPAD_primal_inf_term=0;
		ntheta[1]=1;
		ntheta[0]=1;
	}

	/*_CUDA(cudaMemcpy(x,dev_x,NX*N_NODES*sizeof(real_t),cudaMemcpyDeviceToHost));

		for(int j=0;j<NX*N_NODES;j++){
			fprintf(pFile,"%f ",x[j]);
		}
		fprintf(pFile,"\n");
		_CUDA(cudaMemcpy(u,dev_u,NU*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyDeviceToHost));

		for(int j=0;j<NU*N_NONLEAF_NODES;j++){
			fprintf(pFile,"%f ",u[j]);
		}
		fprintf(pFile,"\n");
		_CUDA(cudaMemcpy(y,dev_y,2*(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));

		for(int j=0;j<2*(NC*N_NONLEAF_NODES+GN_NUMEL);j++){
			fprintf(pFile,"%f ",y[j]);
		}
		fprintf(pFile,"\n");*/

	_CUDA(cudaMemcpy(y,dev_y,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToHost));

	for(int i=0;i<(NC*N_NONLEAF_NODES+GN_NUMEL);i++){
		if(y[i]>0){
			//printf("%d %f ",i,y[i]);
			//fprintf(pFile,"%d %f \n",i,y[i]);
		}
	}
	//printf("\n");
	fclose(pFile);
	printf("BYE HAPPY WORKING WITH SGPAD \n");
	cublasDestroy(handle);
	free(y);
	free(x);
	free(u);
	free(ptr_x);
	free(ptr_w);
	free(ptr_y);
	free(ptr_u);
	free(ptr_q);
	free(ptr_lambda);
	free(grad);
	free(prm_cst_value);
	free(dual_cst_value);
	free(prm_inf);
	free(ptr_Q);
	free(ptr_R);
	free(ptr_temp_ucst);
	free(ptr_temp_xcst);


	_CUDA(cudaFree(dev_q));
	_CUDA(cudaFree(dev_lambda));
	_CUDA(cudaFree(dev_x));
	_CUDA(cudaFree(dev_y));
	_CUDA(cudaFree(dev_w));
	_CUDA(cudaFree(dev_u));
	_CUDA(cudaFree(dev_primal));
	_CUDA(cudaFree(dev_primal_avg));
	_CUDA(cudaFree(dev_primal_iterate));
	_CUDA(cudaFree(dev_Q));
	_CUDA(cudaFree(dev_R));
	_CUDA(cudaFree(dev_temp_cst));

	_CUDA(cudaFree(ptr_dev_q));
	_CUDA(cudaFree(ptr_dev_lambda));
	_CUDA(cudaFree(ptr_dev_x));
	_CUDA(cudaFree(ptr_dev_w));
	_CUDA(cudaFree(ptr_dev_u));
	_CUDA(cudaFree(ptr_dev_Q));
	_CUDA(cudaFree(ptr_dev_R));
	_CUDA(cudaFree(ptr_dev_temp_xcst));
	_CUDA(cudaFree(ptr_dev_temp_ucst));

	free_GPAD_Batch();
	free_host_mem();
	printf("gpad testing is done \n");
	return 0;
}
