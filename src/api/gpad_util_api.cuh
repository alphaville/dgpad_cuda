//#include "../sys_data.h"
#include "../gpadsys_header.cuh"
#include "helper_cuda.h"
#include"cublas_v2.h"
#ifndef __GPAD_UTIL_API_CUH_
#define __GPAD_UTIL_API_CUH_  /**< Header guard for gpad_uti_api */

#pragma once

#define		DEBUG_WARNING		3 /**< All warnings and messages are printed */
#define		DEBUG_INFO			2 /**< Information are printed on stderr */
#define		DEBUG_NORMAL		1 /**< Normal verbosity mode */
#define		DEBUG_SILENT		0 /**< Silent mode */

#define GPAD_DEBUG_LEVEL DEBUG_WARNING

/**** Constant device variables ****/
/**
 * Stages of the tree (constant device data).
 */
__constant__ short DEV_CONST_TREE_STAGES[N_NODES];
/**
 * Nodes per stage (constant device data).
 */
__constant__ short DEV_CONST_TREE_NODES_PER_STAGE[N + 1];
/**
 * Leave nodes of the tree (constant device data).
 */
__constant__ short DEV_CONST_TREE_LEAVES[K];
/**
 * Children of each node (constant device data).
 */
__constant__ short DEV_CONST_TREE_CHILDREN[N_CHILDREN_TOT];
/*
 * Cumulative nodes until the stage
 */
__constant__ short DEV_CONSTANT_TREE_NODES_PER_STAGE_CUMUL[N+2];
/*
 * Number of children for each node (except the leave node)
 */
__constant__ short DEV_CONSTANT_TREE_NUM_CHILDREN[N_NONLEAF_NODES];
/*
 * Cumulative children until the stage.
 */
__constant__ short DEV_CONSTANT_TREE_N_CHILDREN_CUMUL[N_NODES];
/*
 * Ancestor of each node in the tree.
 */
__constant__ short DEV_CONSTANT_TREE_ANCESTOR[N_NODES];

/**** Global WC variables ****/

real_t *dev_A;                   /**<System matrix A*/
real_t *dev_B;					 /**<System matrix B*/
real_t *DEV_CONSTANT_TREE_VALUE; /**< The value of demand in the tree*/
real_t *dev_F;					 /**<Constraint Matrix F*/
real_t *dev_G;					 /**<Constraint Matrix G*/
real_t *dev_g;					 /**<Constraints g*/
real_t *dev_FN;					 /**<Terminal Constraints FN*/
real_t *dev_GN;				     /**<Terminal Constratins GN*/
real_t *dev_GPAD_K_GAIN; 	/**< K allocated on the device write-combined memory */
real_t *dev_GPAD_SIGMA; 	/**< Sigma allocated on the device write-combined memory */
real_t *dev_GPAD_PHI; 		/**< Phi allocated on the device write-combined memory */
real_t *dev_GPAD_THETA; 	/**< Theta allocated on the device write-combined memory */
real_t *dev_GPAD_C; 		/**< C allocated on the device write-combined memory */
real_t *dev_GPAD_D; 		/**< D allocated on the device write-combined memory */
real_t *dev_GPAD_H; 		/**< H allocated on the device write-combined memory */
real_t *dev_GPAD_F; 		/**< F allocated on the device write-combined memory */


/* size of the matrices passed for multiplication */
uint_t nrows[5]={NU,NU,NC,NX,NU};
uint_t ncols[5]={NC,NX,NX,NX,NX};
real_t bet=0.0;
real_t al=1.0;
real_t al_n=-1.0;
real_t hessian_const;
real_t dual_gap=5e-3;
real_t primal_inf=0.05;
uint_t GPAD_iterates=0;
uint_t GPAD_first_cond_flag=0;
uint_t GPAD_second_cond_flag=0;
real_t GPAD_primal_inf_term;
int index_min;



/**** Global variables ****/
real_t *dev_x; 		/**< The state sequence x for all time stages and all nodes */
real_t *dev_u; 		/**< The sequence of all control actions */
real_t *dev_y; 		/**< The dual vector y */
real_t *dev_w;      /**< Accelerated dual vector*/
real_t *dev_q;      /**< Linear cost in the cost> */
real_t *dev_lambda;  /** < Summation of the linear cost of the children at each stage> */
real_t *dev_primal;
real_t *dev_primal_avg;
real_t *dev_primal_iterate;
real_t ntheta[2]={1,1}; /** theta, the accelerating variable*/

real_t *grad;    /** Gradient at each stage */
real_t prm_cst[2]; /** Primal cost at each stage*/
real_t dual_cst[2]; /** Dual cost at each stage*/
real_t *dev_Q;   /** Primal cost matrix Q*/
real_t *dev_R;   /** Primal cost matrix R*/
real_t *dev_temp_cst; /** Temporary variable used in calculation of primal cost */


/**** Global pointers variables****/

const real_t **ptr_dev_GPAD_K_GAIN; 	/**< K allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_SIGMA; 	/**< Sigma allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_PHI; 		/**< Phi allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_THETA; 	/**< Theta allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_C; 		/**< C allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_D; 		/**< D allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_H; 		/**< H allocated on the device write-combined memory */
const real_t **ptr_dev_GPAD_F; 		/**< F allocated on the device write-combined memory */
const real_t **ptr_dev_F;
const real_t **ptr_dev_G;

const real_t **ptr_dev_Q; /** pointer of primal cost matrix Q*/
const real_t **ptr_dev_R; /** pointer of primal cost matrix R*/
real_t **ptr_dev_temp_xcst; /** pointer of temporary cost of the primal cost*/
real_t **ptr_dev_temp_ucst; /** pointer of temporary cost of the primal cost*/

/**** Global variables pointers****/
const real_t **ptr_dev_x; 		/**< The state sequence x for all time stages and all nodes */
real_t **ptr_dev_u; 		/**< The sequence of all control actions */
const real_t **ptr_dev_w; 		/**< The accelerated dual vector y */
real_t **ptr_dev_y; 		/**< The accelerated dual vector y */
real_t **ptr_dev_q;      /**< Linear cost in the cost> */
const real_t **ptr_dev_lambda;  /** < Summation of the linear cost of the children at each stage> */
const real_t **ptr_dev_lambda_N;  /** < Summation of the linear cost of the children at each stage> */
const real_t **ptr_dev_s;

/* Function definitions */

/**
 * \brief Index to access the tree values (w).
 *
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_tree_value(const size_t k, const size_t i);

/**
 * \brief Index to access the matrices K_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_K_gain(const size_t k,const size_t i);

/**printf("%d \n",k);
 * \brief Index to access phi_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_phi(const size_t k, const size_t i);
/**
 * \brief Index to access sigma_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_sigma(const size_t k, const size_t i);

/**
 * \brief Index to access Theta_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_theta(const size_t k, const size_t i);

/**
 * \brief Index to access c_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_c(const size_t k, const size_t i);

/**
 * \brief Index to access d_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_d(const size_t k, const size_t i);

/**
 * \brief Index to access h_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_h(const size_t k, const size_t i);

/**
 * \brief Index to access f_{k}^{(i)}
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_f(const size_t k, const size_t i);

/**
 * \brief Index to access probability p_{k}^{(i)} for given k and i.
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_prob(const size_t k, const size_t i);

/**
 * \brief Number of children of the i-th node at stage k.
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t number_of_children(const size_t k, const size_t i);

/**
 * \brief y (dual variable) of the i-th node at stage k.
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_y(const size_t k, const size_t i);

/**
 * \brief lambda (dual variable) of the i-th node at stage k
 *
 * @param k - Stage index (0, ..., N)
 * @param i - Index of the node at stage k (1, ..., mu_{k})
 */
size_t idx_lambda(const size_t k, const size_t i);
/**
 * \brief Initialize GPAD.
 *
 * Note that this method calls #init_const_mem.
 */
int init_GPAD_Batch(void);

/**
 * \brief To be called upon termination.
 *
 * Frees the allocated memory and terminates the
 * application gracefully. Should be invoked before
 * the application exits.
 */
int free_GPAD_Batch(void);

/**
 * \brief Initializes the constant memory on the device.
 *
 * Constant memory is used for tree and system data.
 * Note that this function is called by #init_GPAD,
 * so there is no need to call it in your application.
 */
void init_const_mem(void);

/**
 * Performs the projection of a vector on the positive
 * orthant. The vector/array can be of any numerical type T
 * which is defined by a typedef template.
 *
 * @param	dx	input-output vector (the result will be stored
 * 				in dx itself)
 * @param	n	number of elements of dx
 * @tparam	T	type of dx (double, int, short, etc)
 *
 */
template<typename T> __global__ void vector_projection(T *dx, int n);

/**
 * Same as vector_projection, but the result is copied
 * to a separate vector dy.
 * @param	dx	input vector
 * @param   dy  projection of dx on the positive orthant (output)
 * @param	n	number of elements of dx
 * @tparam	T	type of dx (double, int, short, etc)
 */
template<typename T> __global__ void vector_projection_copy(T *dx,
		T *dy);


/**
 * Function accelerated_dual_update
 * This kernel is used to calculate the accelerated dual vector
 * w_{v}=y_{v}+\theta_{v}(\theta_{v-1}^{-1}-1)(y_{v}-y_{v-1})
 * and update y_{v-1} with y_{v}
 *
 * @param     y1                 y_{v}
 * @param     y2                 y_{v-1}
 * @param     alpha              \theta_{v}*(\theta_{v-1}^{-1}-1)
 * @param     w                  w_v
 */
template<typename T>__global__ void accelerated_dual_update(T *y1,T *y2,T *w,T alpha);
/**
 *  Function sum_lambda
 * This kernel is called when the scenarios increase from N to N+1 stage
 * else no need to modify q. In this kernel, the no-of blocks are the nodes in the stage and thread Nx (size of x).
 * This kernel do the summation and stores in lambda.
 *
 * Performs the summation of q in the Algorithm 8 of backward substitution
 *
 * @param   q             The previous q and qN=yN;
 * @param   lambda        This is where the summations are stored
 * @param   stage         The stage at which the function is called [0, N-1];
 *         no of threads   size of x
 *         no of blocks    no of nodes at the stage
 */

template<typename T> __global__ void sum_lambda(const T *q,
		T *lambda,
		int stage);


/**
 * Function parallel_sum
 * In this function the summation of vector a, and b of a specified size is performed.
 * This function can be performed in NB blocks and NT threads
 * The element of the vector is given as vec_ele=blockIdx.x*blockDim.x+threadIdx.x
 * If this element vec_ele is greater than size no action is performed.
 *
 * @param       a       destination vector
 * @param       b       source vector
 * @tparam      T       data type
 *
 * a=a+b
 */
template<typename T>__global__ void parallel_sum(T *a,const T *b,int size);

/**
 * Function copy_vectors.
 *
 * In this function, vector b is copied to vector b of a specified size is performed.
 * This function can be performed in NB blocks and NT threads
 * The element of the vector is given as vec_ele=blockIdx.x*blockDim.x+threadIdx.x
 * If this element vec_ele is greater than size no action is performed.
 * @param         a       destination vector
 * @param         b       source vector
 * @tparam        T       data type
 */
template<typename T>__global__ void copy_vectors(T *a,T *b,int size);

/**
 *Function summation_child
 *In this function, the value of the next state is updated according.
 *@param        x        Actual state
 *@param        y        updated state
 *@param     stage       stage in the tree.
 */
template<typename T>__global__ void summation_child(T *x,T *y,T *DEV_CONSTANT_TREE_VALUE,int stage);

/**
 *  Function projection_NONLeaves
 *  In this function the dual vector at all the non-leaves is projected on to the
 *  positive number
 *  y_{v+1}=[w_{v}+\alpha*(x-g)
 *  @param        x       dual vector
 *  @param        y       g in the constraints
 *  @param        w       accelerated dual vector
 *  @param   primal       calculates g(z)
 *  @param    alpha       Lipzitz constant
 */
template<typename T> __global__ void projection_NONLeaves(T *x,T *y,T *w,T *primal,T alpha);

/**
 *  Function projection_leaves
 *  In this function the dual vector at all the leaves is projected on to the
 *  positive number
 *  y_{v+1}=[w_{v}+\alpha*(x-g)
 *  @param        x       dual vector
 *  @param        y       gN in the constraints
 *  @param        w       accelerated dual vector
 *  @param   primal       calculates g(z)
 *  @param    alpha       Lipzitz constant
 */
template<typename T>__global__ void projection_leaves(T *x,T *y,T *w,T *primal,T alpha);
/**
 *  Function Find max
 *  In this function the max element in the vecot is found and store in
 *  vector y. The first element of y is the maximum element.
 *  @param         x       Actual vector
 *  @param         y       Vector in Descending order
 *  @param    length       The length of the vector
 *  @param    nthreads     half of the length
 */
template<typename T>__global__ void find_max(T *x,T *y,int lenght,int nthreads);
/**
 * Function multiple_mv_sum
 * This function perform the matrix-vector multiplications and the summation of the result
 *
 * Z=A*x+B*y+....K*p is implemented in the function
 *
 * @param    matrices      Array containing the pointers to the martices.
 * @param           x      Array containing the pointers to the vectors.
 * @param       nrows      The rows of each of the matrices.
 * @param       ncols      The columns of each of the matrices.
 * @param   nmatrices      No of matrix vector multiplications
 * @param       state      This vector contains {1,-1} represented whether A*x or A'*x performed.
 * @param           y      The result is stored in the vector y.
 * @param      handle      cublashandle
 */
template<typename T>__host__ void multiple_mv_sum(T **matrices,T **x,
		const int *nrows,const int *ncols,int nmatrices, int *state,T *y,cublasHandle_t handle);

/**
 * Function dynamic_GPAD
 * This function performs the dynamic programming step in the GPAD algorithm
 *
 * @param        y            The dual variables
 * @param        s            The constant part in the control input.
 * @param        q            The linear part in the cost function.
 * @param   lambda            Used to store the summation of the q at every node.
 * @param        x            Contains the optimal \star{x}.
 * @param        u            Contains the optimal \star{u}.
 * @param   handle            Cublas handler for cublas operations.
 */
template<typename T> __host__ void dynamic_GPAD_Batch(const T **ptr_y,T **ptr_q,
		const T **ptr_lambda,const T **ptr_s,const T **ptr_x,T **ptr_u,cublasHandle_t handle);
/**
 * Function control calculate
 * The control action at a stage is calculated as
 * $u_k^{(i)}= K_kx_k^{(i)}+s_k^{(i)}$ for all i \in the nodes at this stage
 * We can observe that this multiplication can be done as
 * u_k=K_k*[x_k^{(1)} x_k^{(2)} \ldots x_k^{(n)}] + [s_k^{(1)} s_k^{(2)} \ldots s_k^{(n)}];
 *
 * @param        K          The state-control gain
 * @param        s          Linear part in the control input
 * @param     node          Number of nodes in the stage
 * @param        u          control at all the nodes
 * @param   handle          Cublas handler for cublas operations.
 */
template<typename T>__host__ void control_calculate(T* x,const int stage, T* u,cublasHandle_t handle);

/**
 * Function system_update
 * x_{k+1}^{(i)}=Ax_k^{(i)}+Bu_k^{(i)}+w_k^i
 * This performs the system update for all the nodes at the stage
 * @param        x        state of the system
 * @param        u        control of the system
 * @param        w        disturbance on the node
 * @param     node        number of nodes on stage k
 * @param  handle        cublashandle
 */
template<typename T>__host__ void system_update(T *x,T *u,T *lambda,int stage,cublasHandle_t handle);

/**
 * Function dual_update by projection on the positive horizon
 * y_{v+1}=[w_v+\alpha*g(z_{v})]_{+}
 *
 */
template<typename T>__host__ void dual_update(T *w,T *x,T *u,T *y,cublasHandle_t handle);

/*
 * Function cal_dual_grad
 * This function calculate the gradient to check the restart condition
 * g(y_v)'(y_{v+1}-y_{v})>0
 * @param         y      The dual variable
 * @param         g      The gradient g(y_v)
 * @param    handle      cublas handler
 */

template<typename T>__host__ void gradient_restart(T *y,T *g,cublasHandle_t handle);
/*
 * This function implements the GPAD algorithm.
 * @param       x     initial state of the system.
 */
template<typename T>__host__ void GPAD_algorithm(T *x);

/*
 * This function implements the GPAD algorithm with monotonicity enforcement.
 * @param       x     initial state of the system.
 */
template<typename T>__host__ void Mono_GPAD_algorithm(T *x);

/*
 * This function implements the GPAD algorithm with restarting the algorithm.
 * @param       x     initial state of the system.
 */
template<typename T>__host__ void Restart_GPAD_algorithm(T *x);
/*
 * primal_cost_function
 * This function implements the primal cost function.
 *
 */
template<typename T>__host__ void primal_cost_function(cublasHandle_t handle);

#endif /* __GPAD_UTIL_CUH_ */
