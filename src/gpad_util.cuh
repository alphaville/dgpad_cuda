#include "api/gpad_util_api.cuh"
#include "error_handles.cuh"
#include "cuda_timer.cuh"
#include"cublas_v2.h"
#include<assert.h>
#ifndef __GPAD_UTIL_CUH_
#define __GPAD_UTIL_CUH_
/*** STATIC function definitions ***/
static inline void verify_ki_1(const size_t k, const size_t i);
static inline void verify_ki_2(const size_t k, const size_t i);

#define VERIFY_KI1(k,i) verify_ki_1((k), (i), __FILE__, __LINE__)
#define VERIFY_KI2(k,i) verify_ki_1(k,i,__FILE__, __LINE__)

/**** Implementations ****/
void init_const_mem(void) {
	/* Constant memory allocation */
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONST_TREE_STAGES, TREE_STAGES,
					N_NODES * sizeof(*DEV_CONST_TREE_STAGES)));
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONST_TREE_NODES_PER_STAGE,
					TREE_NODES_PER_STAGE,
					(N + 1) * sizeof(*DEV_CONST_TREE_NODES_PER_STAGE)));
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONST_TREE_LEAVES, TREE_STAGES,
					N_NODES * sizeof(*DEV_CONST_TREE_LEAVES)));
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONSTANT_TREE_NODES_PER_STAGE_CUMUL,
					TREE_NODES_PER_STAGE_CUMUL,
					(N + 2)*sizeof(short)));
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONSTANT_TREE_NUM_CHILDREN,
					TREE_NUM_CHILDREN,
					(N_NONLEAF_NODES)*sizeof(short)));
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONSTANT_TREE_N_CHILDREN_CUMUL,
					TREE_N_CHILDREN_CUMUL,
					N_NODES*sizeof(short)));
	checkCudaErrors(
			cudaMemcpyToSymbol(DEV_CONSTANT_TREE_ANCESTOR,
					TREE_ANCESTOR,
					N_NODES*sizeof(short)));
}

template<typename T> __global__ void vector_projection(T * dx, int n) {
	int tid;
	tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < n) { /* guard */
		if (dx[tid] < 0)
			dx[tid] = (T) 0;
	}
}

template<typename T> __global__ void vector_projection_copy(const T *dx, T *dy,
		int n) {
	int tid;
	tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < n) { /* guard */
		if (dx[tid] < 0) {
			dy[tid] = (T) 0;
		} else {
			dy[tid] = dx[tid];
		}
	}
}

template<typename T>__global__ void accelerated_dual_update(T *y1,T *y2,T *w,T alpha){
	int tid=threadIdx.x+blockIdx.x*blockDim.x;
	//int tid=threadIdx.x;
	if(tid<(NC*N_NONLEAF_NODES+GN_NUMEL)){
		real_t temp=(y1[tid]-y2[tid]);
		w[tid]=y1[tid]+alpha*temp;
		y2[tid]=y1[tid];
		threadfence();
	}
}
template<typename T> __global__ void sum_lambda(const T *q, T *lambda,int stage){

	int Pnode=(int)DEV_CONSTANT_TREE_NODES_PER_STAGE_CUMUL[stage];
	int bid=blockIdx.x;
	int tid=threadIdx.x;
	int q_start_idx=(int)(DEV_CONSTANT_TREE_N_CHILDREN_CUMUL[Pnode+bid]-
			DEV_CONSTANT_TREE_N_CHILDREN_CUMUL[Pnode])*blockDim.x+tid;
	int nchild=(int)DEV_CONSTANT_TREE_NUM_CHILDREN[Pnode+bid];
	int lambda_strt = bid * blockDim.x+tid;
	for (int i = 0; i < nchild - 1; i++) {
		if (i >0)
			lambda[lambda_strt] =
					lambda[lambda_strt]+ q[q_start_idx + (i + 1) * blockDim.x];
		else
			lambda[lambda_strt] = q[q_start_idx+i * blockDim.x]+ q[q_start_idx + (i + 1) * blockDim.x];
	}
	__threadfence_block();
}
template<typename T>__global__ void parallel_sum(T *a,const T *b,int size){
	int vec_ele=blockIdx.x*blockDim.x+threadIdx.x;
	if(vec_ele<size)
		a[vec_ele]=a[vec_ele]+b[vec_ele];
}
template<typename T>__global__ void copy_vectors(T *a,T *b,int size){
	int vec_ele=blockIdx.x*blockDim.x+threadIdx.x;
	if(vec_ele<size)
		a[vec_ele]=b[vec_ele];
}

template<typename T>__global__ void summation_child(T *x,T *y,T *DEV_CONSTANT_TREE_VALUE,int stage){
	int bid=blockIdx.x;
	int tid=threadIdx.x;
	int Pnode=DEV_CONSTANT_TREE_NODES_PER_STAGE_CUMUL[stage+2]-1;
	int pre_loc=(DEV_CONSTANT_TREE_NODES_PER_STAGE_CUMUL[stage+1])*NX;
	int Pre_node=DEV_CONSTANT_TREE_NODES_PER_STAGE_CUMUL[stage+1]-1;
	int x_strt=(DEV_CONSTANT_TREE_ANCESTOR[Pnode-bid]-DEV_CONSTANT_TREE_ANCESTOR[Pre_node]-1)*NX;
	x[(gridDim.x-1-bid)*blockDim.x+tid]=y[x_strt+tid]+DEV_CONSTANT_TREE_VALUE[pre_loc+(gridDim.x-1-bid)*blockDim.x+tid];
}
template<typename T> __global__ void projection_NONLeaves(T *x,T *y,T *w,T *primal,T alpha){
	int tid=threadIdx.x;
	//if(gridDim.x<N_NONLEAF_NODES+1){
	int bid=blockIdx.x*blockDim.x+tid;
	if(tid<NC){
		primal[bid]=x[bid]-y[bid];
		x[bid]=w[bid]+alpha*(x[bid]-y[bid]);
		if(x[bid]<0){
			x[bid]=(T) 0;
		}
	}
	//}
}

template<typename T>__global__ void projection_leaves(T *x,T *y,T *w,T *primal,T alpha){
	int tid=blockIdx.x*blockDim.x+threadIdx.x;
	if(tid<GN_NUMEL){
		primal[tid]=(x[tid]-y[tid]);
		x[tid]=w[tid]+alpha*(x[tid]-y[tid]);
		if(x[tid]<0){
			x[tid]=(T) 0;
		}
	}
}

template<typename T>__global__ void find_max(T *x,T *y,int lenght,int nthreads){
	int tid=threadIdx.x+blockIdx.x*blockDim.x;
	int Lvector=lenght;
	int Tthreads=nthreads;
	while(Tthreads>0){
		if(tid<Tthreads){
			if(Lvector==lenght){
				if((Lvector%2)==0){
					y[tid]=(x[tid]>x[tid+Tthreads] ? x[tid]:x[tid+Tthreads]);
				}else{
					if(tid==Tthreads-1){
						y[tid]=(x[tid]>x[tid+Tthreads] ? x[tid]:x[tid+Tthreads]);
						y[tid]=(y[tid]>x[tid+Tthreads+1] ? y[tid]:x[tid+Tthreads+1]);
					}else{
						y[tid]=(x[tid]>x[tid+Tthreads] ? x[tid]:x[tid+Tthreads]);
					}
				}
				Lvector=Lvector/2;
				Tthreads=Tthreads/2;
			}else{
				if((Lvector%2)==0){
					y[tid]=(y[tid]>y[tid+Tthreads] ? y[tid]:y[tid+Tthreads]);
				}else{
					if(tid==Tthreads-1){
						y[tid]=(y[tid]>y[tid+Tthreads] ? y[tid]:y[tid+Tthreads]);
						y[tid]=(y[tid]>y[tid+Tthreads+1] ? y[tid]:y[tid+Tthreads+1]);
					}else{
						y[tid]=(y[tid]>y[tid+Tthreads] ? y[tid]:y[tid+Tthreads]);
					}
				}
				Lvector=Lvector/2;
				Tthreads=Tthreads/2;
			}
		}else{
			Tthreads=0;
		}
		__threadfence();
	}
}


int init_GPAD_Batch(){

	init_const_mem();

	_CUDA(cudaMalloc((void**)&dev_A,NX*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_B,NU*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_F,N_NONLEAF_NODES*NC*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_G,N_NONLEAF_NODES*NC*NU*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_g,NC*N_NONLEAF_NODES*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_FN,GN_NUMEL*NX*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&dev_GN,GN_NUMEL*sizeof(real_t)));
	_CUDA(cudaMalloc((void**)&DEV_CONSTANT_TREE_VALUE,NX*N_NODES*sizeof(real_t)));

	/* Device variables - declarations */
	const int n_cuda_vars = 8;

	/* Construct the streams */
	cudaStream_t* copy_streams = (cudaStream_t*) malloc(
			n_cuda_vars * sizeof(cudaStream_t)); // Creation of data streams
	if (copy_streams == NULL) {
		fprintf(stderr, "Allocation error at line %d of %s \n", __LINE__,
				__FILE__);
		exit(500);
	}
	for (int i = 0; i < n_cuda_vars; i++) {
		checkCudaErrors(cudaStreamCreate(&(copy_streams[i])));
	}

	/* Memory allocation on the host [cudaMallocHost] 	*/
	/* as write-combined memory 						*/
	//cudaHostAllocWriteCombined
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_K_GAIN,
					DIM_GPAD_K_GAIN * sizeof(*dev_GPAD_K_GAIN)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_SIGMA,
					N_NONLEAF_NODES * NU * sizeof(*dev_GPAD_SIGMA)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_PHI,
					N_NONLEAF_NODES * NC * NU
					* sizeof(*dev_GPAD_PHI)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_THETA,
					((N_NONLEAF_NODES-K)*NX*NU+(GN_NUMEL)*NU) * sizeof(*dev_GPAD_THETA)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_C,
					N_NONLEAF_NODES * NX * sizeof(*dev_GPAD_C)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_D,
					N_NONLEAF_NODES * NC * NX * sizeof(*dev_GPAD_D)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_H,
					N_NONLEAF_NODES * NU * NX * sizeof(*dev_GPAD_H)));
	checkCudaErrors(
			cudaMalloc((void**) &dev_GPAD_F,
					((N_NONLEAF_NODES-K)*NX*NX+(GN_NUMEL)*NX)* sizeof(*dev_GPAD_F)));

	/* Copy data onto the device [Asynchronously] */
	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_K_GAIN, GPAD_K_GAIN,
					DIM_GPAD_K_GAIN * sizeof(*dev_GPAD_K_GAIN),
					cudaMemcpyHostToDevice, copy_streams[0]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_SIGMA, GPAD_SIGMA,
					N_NONLEAF_NODES * NU * sizeof(*dev_GPAD_SIGMA),
					cudaMemcpyHostToDevice, copy_streams[1]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_PHI, GPAD_PHI,
					N_NONLEAF_NODES * NC * NU
					* sizeof(*dev_GPAD_PHI), cudaMemcpyHostToDevice,
					copy_streams[2]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_THETA, GPAD_THETA,
					((N_NONLEAF_NODES-K)*NX*NU+(GN_NUMEL)*NU)  * sizeof(*dev_GPAD_THETA),
					cudaMemcpyHostToDevice, copy_streams[3]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_C, GPAD_C,
					N_NONLEAF_NODES * NX * sizeof(*dev_GPAD_C),
					cudaMemcpyHostToDevice, copy_streams[4]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_D, GPAD_D,
					N_NONLEAF_NODES * NC * NX * sizeof(*dev_GPAD_D),
					cudaMemcpyHostToDevice, copy_streams[5]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_H, GPAD_H,
					N_NONLEAF_NODES * NU * NX * sizeof(*dev_GPAD_H),
					cudaMemcpyHostToDevice, copy_streams[6]));

	checkCudaErrors(
			cudaMemcpyAsync(dev_GPAD_F, GPAD_F,
					((N_NONLEAF_NODES-K)*NX*NX+(GN_NUMEL)*NX) * sizeof(*dev_GPAD_F),
					cudaMemcpyHostToDevice, copy_streams[7]));

	/* Synchronize [and check for errors] */
	checkCudaErrors(cudaDeviceSynchronize());

	/* Destroy the copy streams */
	for (int i = 0; i < n_cuda_vars; i++) {
		checkCudaErrors(cudaStreamDestroy(copy_streams[i]));
	}
	/* Free memory for copy_streams */
	if (copy_streams)
		free(copy_streams);

	_CUDA(cudaMemcpy(dev_A,A,NX*NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_B,B,NX*NU*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_F,F,N_NONLEAF_NODES*NC*NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_G,G,N_NONLEAF_NODES*NC*NU*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_g,g,NC*N_NONLEAF_NODES*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_FN,FN,GN_NUMEL*NX*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(dev_GN,gN,GN_NUMEL*sizeof(real_t),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(DEV_CONSTANT_TREE_VALUE,TREE_VALUE,NX*N_NODES*sizeof(real_t),cudaMemcpyHostToDevice));

	/** Host pointers*/
	real_t **ptr_GPAD_K_GAIN=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_SIGMA=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_PHI=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_THETA=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_C=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_D=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_H=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_GPAD_F=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_F=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));
	real_t **ptr_G=(real_t**)malloc(N_NONLEAF_NODES*sizeof(real_t*));

	/** Device pointers */
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_K_GAIN,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_SIGMA,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_PHI,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_THETA,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_C,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_D,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_H,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_GPAD_F,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_F,N_NONLEAF_NODES*sizeof(real_t*)));
	_CUDA(cudaMalloc((void**)&ptr_dev_G,N_NONLEAF_NODES*sizeof(real_t*)));

	for(int i=0;i<N_NONLEAF_NODES;i++){
		ptr_GPAD_K_GAIN[i]=&dev_GPAD_K_GAIN[i*NX*NU];
		ptr_GPAD_SIGMA[i]=&dev_GPAD_SIGMA[i*NU];
		ptr_GPAD_PHI[i]=&dev_GPAD_PHI[i*NU*NC];
		ptr_GPAD_C[i]=&dev_GPAD_C[i*NX];
		ptr_GPAD_D[i]=&dev_GPAD_D[i*NX*NC];
		ptr_GPAD_H[i]=&dev_GPAD_H[i*NX*NU];
		ptr_F[i]=&dev_F[i*NC*NX];
		ptr_G[i]=&dev_G[i*NC*NU];
		if(i<N_NONLEAF_NODES-K){
			ptr_GPAD_F[i]=&dev_GPAD_F[i*NX*NX];
			ptr_GPAD_THETA[i]=&dev_GPAD_THETA[i*NU*NX];
		}
		else{
			ptr_GPAD_THETA[i]=&dev_GPAD_THETA[(N_NONLEAF_NODES-K)*NU*NX+(i-N_NONLEAF_NODES+K)*FN_ROWS[1]*NU];
			ptr_GPAD_F[i]=&dev_GPAD_F[(N_NONLEAF_NODES-K)*NX*NX+(i-N_NONLEAF_NODES+K)*FN_ROWS[1]*NX];
		}
	}

	_CUDA(cudaMemcpy(ptr_dev_GPAD_K_GAIN,ptr_GPAD_K_GAIN,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_SIGMA,ptr_GPAD_SIGMA,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_PHI,ptr_GPAD_PHI,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_THETA,ptr_GPAD_THETA,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_C,ptr_GPAD_C,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_D,ptr_GPAD_D,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_H,ptr_GPAD_H,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_GPAD_F,ptr_GPAD_F,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_F,ptr_F,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));
	_CUDA(cudaMemcpy(ptr_dev_G,ptr_G,N_NONLEAF_NODES*sizeof(real_t*),cudaMemcpyHostToDevice));

	free(ptr_GPAD_K_GAIN);
	free(ptr_GPAD_SIGMA);
	free(ptr_GPAD_PHI);
	free(ptr_GPAD_THETA);
	free(ptr_GPAD_C);
	free(ptr_GPAD_D);
	free(ptr_GPAD_H);
	free(ptr_GPAD_F);
	free(ptr_F);
	free(ptr_G);
	return 0;
}

template<typename T> __host__ void dynamic_GPAD_Batch(const T **ptr_w,T **ptr_q,
		const T **ptr_lambda,const T **ptr_s,const T **ptr_x,T **ptr_u,cublasHandle_t handle){

	//Backward Iteration.
	for(int k=N-1;k>-1;k--){
		if(TREE_NODES_PER_STAGE[k]<TREE_NODES_PER_STAGE[k+1]){
			sum_lambda<real_t><<<TREE_NODES_PER_STAGE[k],NX>>>(dev_q,dev_lambda,k);
		}
		if(k==N-2){
			ncols[1]=NX;
			nrows[3]=NX;
		}
		if(k==N-1){
			ncols[1]=FN_ROWS[0];
			nrows[3]=FN_ROWS[0];
			/*printf("%d %d %d ",k,nrows[1],ncols[1]);
			_CUDA(cudaMemcpy(z,&ptr_dev_GPAD_THETA[TREE_NODES_PER_STAGE_CUMUL[k]],TREE_NODES_PER_STAGE[k]*sizeof(real_t*),cudaMemcpyDeviceToHost));
			printf("%p ",z);
			checkCudaErrors(cudaMemcpy(t,z[0],nrows[1]*ncols[1]*sizeof(real_t),cudaMemcpyDeviceToHost));
			for(int j=0;j<ncols[1]*nrows[1];j++){
				printf("%f ",t[j]);
			}
			printf("\n");
			_CUDA(cudaMemcpy(z,&ptr_dev_GPAD_F[TREE_NODES_PER_STAGE_CUMUL[k]],TREE_NODES_PER_STAGE[k]*sizeof(real_t*),cudaMemcpyDeviceToHost));
			printf("%p ",z);
			checkCudaErrors(cudaMemcpy(t,z[0],nrows[3]*ncols[3]*sizeof(real_t),cudaMemcpyDeviceToHost));
			for(int j=0;j<ncols[3]*nrows[3];j++){
				printf("%f ",t[j]);
			}
			printf("\n");*/
			_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,nrows[1],1,ncols[1],&al,&ptr_dev_GPAD_THETA[TREE_NODES_PER_STAGE_CUMUL[k]],
					nrows[1],ptr_dev_lambda_N,ncols[1],&bet,&ptr_u[TREE_NODES_PER_STAGE_CUMUL[k]],nrows[1],TREE_NODES_PER_STAGE[k]));
			_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,ncols[3],1,nrows[3],&al,&ptr_dev_GPAD_F[TREE_NODES_PER_STAGE_CUMUL[k]],
					ncols[3],ptr_dev_lambda_N,nrows[3],&bet,ptr_q,ncols[3],TREE_NODES_PER_STAGE[k]));

			/*for(int i=1;i<TREE_NODES_PER_STAGE[k]+1;i++){
				_CUBLAS(cublasSgemv(handle, CUBLAS_OP_N,nrows[1],ncols[1],&al,&dev_GPAD_THETA[idx_theta(k,i)],nrows[1],
						&dev_lambda[idx_lambda(k,i)],1,&bet,&dev_u[(TREE_NODES_PER_STAGE_CUMUL[k]+i-1)*NU],1));
				_CUBLAS(cublasSgemv(handle, CUBLAS_OP_N,ncols[3],nrows[3],&al,&dev_GPAD_F[idx_f(k,i)],ncols[3],
						&dev_lambda[idx_lambda(k,i)],1,&bet,&dev_q[(i-1)*NX],1));
			}*/
		}
		else{
			_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,nrows[1],1,ncols[1],&al,&ptr_dev_GPAD_THETA[TREE_NODES_PER_STAGE_CUMUL[k]],
					nrows[1],ptr_lambda,ncols[1],&bet,&ptr_u[TREE_NODES_PER_STAGE_CUMUL[k]],nrows[1],TREE_NODES_PER_STAGE[k]));
			_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,ncols[3],1,nrows[3],&al,&ptr_dev_GPAD_F[TREE_NODES_PER_STAGE_CUMUL[k]],
					ncols[3],ptr_lambda,nrows[3],&bet,ptr_q,ncols[3],TREE_NODES_PER_STAGE[k]));
		}
		_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,nrows[0],1,ncols[0],&al,&ptr_dev_GPAD_PHI[TREE_NODES_PER_STAGE_CUMUL[k]],
				nrows[0],&ptr_w[TREE_NODES_PER_STAGE_CUMUL[k]],ncols[0],&al,&ptr_u[TREE_NODES_PER_STAGE_CUMUL[k]],nrows[0],TREE_NODES_PER_STAGE[k]));
		_CUBLAS(cublasSaxpy(handle,NU*TREE_NODES_PER_STAGE[k],&al,&dev_GPAD_SIGMA[TREE_NODES_PER_STAGE_CUMUL[k]*NU],1,&dev_u[TREE_NODES_PER_STAGE_CUMUL[k]*NU],1));
		_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,ncols[2],1,nrows[2],&al,&ptr_dev_GPAD_D[TREE_NODES_PER_STAGE_CUMUL[k]],
				ncols[2],&ptr_w[TREE_NODES_PER_STAGE_CUMUL[k]],nrows[2],&al,ptr_q,ncols[2],TREE_NODES_PER_STAGE[k]));
		_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,ncols[4],1,nrows[4],&al,&ptr_dev_GPAD_H[TREE_NODES_PER_STAGE_CUMUL[k]],
				ncols[4],&ptr_s[TREE_NODES_PER_STAGE_CUMUL[k]],nrows[4],&al,ptr_q,ncols[4],TREE_NODES_PER_STAGE[k]));
		_CUBLAS(cublasSaxpy(handle,NX*TREE_NODES_PER_STAGE[k],&al,&dev_GPAD_C[TREE_NODES_PER_STAGE_CUMUL[k]*NX],1,dev_q,1));
		_CUBLAS(cublasScopy(handle,NX*TREE_NODES_PER_STAGE[k],dev_q,1,dev_lambda,1));
	}
	//Forward Iteration
	for(int k=0;k<N;k++){
		_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,NU,1,NX,&al,&ptr_dev_GPAD_K_GAIN[TREE_NODES_PER_STAGE_CUMUL[k]],
				NU,&ptr_x[TREE_NODES_PER_STAGE_CUMUL[k]],NX,&al,&ptr_u[TREE_NODES_PER_STAGE_CUMUL[k]],NU,TREE_NODES_PER_STAGE[k]));
		system_update<real_t>(&dev_x[TREE_NODES_PER_STAGE_CUMUL[k]*NX],&dev_u[TREE_NODES_PER_STAGE_CUMUL[k]*NU],dev_lambda,k,handle);
	}
}
template<typename T>__host__ void control_calculate(T* x,const int stage, T* u,cublasHandle_t handle){

	real_t al=1.0;
	real_t bet=1.0;
	real_t* h_x=(real_t*)malloc(NU*NX*TREE_NODES_PER_STAGE[stage]*sizeof(real_t));
	for(int i=1;i<TREE_NODES_PER_STAGE[stage]+1;i++)
		_CUBLAS(cublasSgemv(handle, CUBLAS_OP_N,NU,NX,&al,&dev_GPAD_K_GAIN[idx_K_gain(stage,i)],NU,
				&x[(i-1)*NX],1,&bet,&u[(i-1)*NU],1));
}

template<typename T>__host__ void system_update(T *x,T *u,T *lambda,int stage,cublasHandle_t handle){

	int node=TREE_NODES_PER_STAGE[stage];
	_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N,CUBLAS_OP_N,NX,node,NX,&al,dev_A,NX,x,NX,&bet,lambda,NX));
	_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N,CUBLAS_OP_N,NX,node,NU,&al,dev_B,NX,u,NU,&al,lambda,NX));
	summation_child<real_t><<<TREE_NODES_PER_STAGE[stage+1],NX>>>(&x[node*NX],lambda,DEV_CONSTANT_TREE_VALUE,stage);
}


template<typename T>__host__ void dual_update(T *w,T *x,T *u,T *y,cublasHandle_t handle){
	//_CUBLAS(cublasSgemm(handle,CUBLAS_OP_N,CUBLAS_OP_N,NC,N_NONLEAF_NODES,NX,&al,dev_F,NC,x,NX,&bet,y,NC));
	//_CUBLAS(cublasSgemm(handle,CUBLAS_OP_N,CUBLAS_OP_N,NC,N_NONLEAF_NODES,NU,&al,dev_G,NC,u,NU,&al,y,NC));
	_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,NC,1,NX,&al,ptr_dev_F,NC,ptr_dev_x,
			NX,&bet,ptr_dev_y,NC,N_NONLEAF_NODES));
	checkCudaErrors(cudaDeviceSynchronize());
	_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,NC,1,NU,&al,ptr_dev_G,NC,ptr_dev_s,
			NU,&al,ptr_dev_y,NC,N_NONLEAF_NODES));

	_CUDA(cudaMemcpy(dev_primal,dev_y,N_NONLEAF_NODES*NC*sizeof(real_t),cudaMemcpyDeviceToDevice));
	projection_NONLeaves<real_t><<<N_NONLEAF_NODES,NC>>>(y,dev_g,w,dev_primal_iterate,hessian[0]);
	for(int i=1;i<TREE_NODES_PER_STAGE[N-1]+1;i++){
		_CUBLAS(cublasSgemv(handle, CUBLAS_OP_N,FN_ROWS[i-1],NX,&al,&dev_FN[FN_ROWS_CUMUL[i-1]*NX],FN_ROWS[i-1],
				&x[(N_NONLEAF_NODES+i-1)*NX],1,&bet,&y[idx_y(N,i)],1));
	}

	_CUDA(cudaMemcpy(&dev_primal[NC*N_NONLEAF_NODES],&dev_y[NC*N_NONLEAF_NODES],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
	projection_leaves<real_t><<<GN_NUMEL,1>>>(&y[NC*N_NONLEAF_NODES],dev_GN,&w[NC*N_NONLEAF_NODES],
			&dev_primal_iterate[NC*N_NONLEAF_NODES],hessian[0]);
	_CUBLAS(cublasScopy(handle,N_NONLEAF_NODES*NC+GN_NUMEL,y,1,&w[N_NONLEAF_NODES*NC+GN_NUMEL],1));
	_CUBLAS(cublasSaxpy(handle,N_NONLEAF_NODES*NC+GN_NUMEL,&al_n,&y[N_NONLEAF_NODES*NC+GN_NUMEL],1,&w[N_NONLEAF_NODES*NC+GN_NUMEL],1));
}


template<typename T>__host__ void  gradient_restart(T *y,T *g,cublasHandle_t handle){

}


template<typename T>__host__ void GPAD_algorithm(T *x,cublasHandle_t handle){
	real_t alpha;
	real_t primal_infes_iter;
	real_t min_al=-1;
	real_t multiple_prd=1;
	int length=NC*N_NONLEAF_NODES+GN_NUMEL;
	int nthreads=length/2;
	real_t inv_neta=0;

	for(int i=0;i<iterate[0];i++){
		alpha=ntheta[1]*(1/ntheta[0]-1);
		accelerated_dual_update<real_t><<<(N_NONLEAF_NODES+GN_NUMEL),NC>>>(dev_y,&dev_y[N_NONLEAF_NODES*NC+GN_NUMEL],dev_w,alpha);
		_CUDA(cudaMemcpy(dev_q,&dev_w[N_NONLEAF_NODES*NC],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
		_CUDA(cudaMemcpy(dev_lambda,&dev_w[N_NONLEAF_NODES*NC],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
		dynamic_GPAD_Batch<real_t>(ptr_dev_w,ptr_dev_q,ptr_dev_lambda,ptr_dev_s,ptr_dev_x,ptr_dev_u,handle);
		dual_update<real_t>(dev_w,dev_x,dev_u,dev_y,handle);
		_CUBLAS(cublasSdot(handle,N_NONLEAF_NODES*NC+GN_NUMEL,&dev_w[N_NONLEAF_NODES*NC+GN_NUMEL],1,dev_primal_iterate,1,&grad[i]));

		//primal_cost_function<real_t>(handle);
		//prm_cst_value[i]=prm_cst[0];
		//dual_cst_value[i]=dual_cst[0];
		if(i>0){
			multiple_prd=1-ntheta[1];
			_CUBLAS(cublasSscal(handle,NC*N_NONLEAF_NODES+GN_NUMEL,&ntheta[1],dev_primal,1));
			_CUBLAS(cublasSaxpy(handle,NC*N_NONLEAF_NODES+GN_NUMEL,&multiple_prd,dev_primal,1,&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],1));
			_CUBLAS(cublasSaxpy(handle,NC*N_NONLEAF_NODES,&min_al,dev_g,1,&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],1));
			_CUBLAS(cublasSaxpy(handle,GN_NUMEL,&min_al,dev_GN,1,&dev_primal[2*NC*N_NONLEAF_NODES+GN_NUMEL],1));
		}else{
			_CUDA(cudaMemcpy(&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],dev_primal,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToDevice));
			_CUBLAS(cublasSaxpy(handle,NC*N_NONLEAF_NODES,&min_al,dev_g,1,&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],1));
			_CUBLAS(cublasSaxpy(handle,GN_NUMEL,&min_al,dev_GN,1,&dev_primal[2*NC*N_NONLEAF_NODES+GN_NUMEL],1));
		}
		find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NC>>>(&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],dev_primal_avg,length,nthreads);
		_CUDA(cudaMemcpy(&primal_infes_iter,dev_primal_avg,sizeof(real_t),cudaMemcpyDeviceToHost));

		inv_neta=1/ntheta[1];
		_CUBLAS(cublasSscal(handle,NC*N_NONLEAF_NODES+GN_NUMEL,&inv_neta,dev_primal,1));
		_CUDA(cudaMemcpy(&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],dev_primal,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToDevice));
		if(i>5){
			termination_condition<real_t>(primal_infes_iter,i,length,nthreads,handle);
			/*if(primal_infes_iter<primal_inf){
				GPAD_first_cond_flag=1;
				GPAD_iterates=i;
				i=iterate[0]+1;
				GPAD_primal_inf_term=primal_infes_iter;
			}else{
				find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NC>>>(dev_primal_iterate,dev_primal_avg,length,nthreads);
				_CUDA(cudaMemcpy(&primal_infes_iter,dev_primal_avg,sizeof(real_t),cudaMemcpyDeviceToHost));
				prm_inf[i]=primal_infes_iter;
				if(primal_infes_iter<primal_inf){
					_CUBLAS(cublasIsamin(handle,(N_NONLEAF_NODES+GN_NUMEL)*NC,dev_w,1,&index_min));
					_CUDA(cudaMemcpy(&primal_infes_iter,&dev_w[index_min-1],sizeof(real_t),cudaMemcpyDeviceToHost));
					if(primal_infes_iter>=0){
						_CUBLAS(cublasSdot(handle,NC*N_NONLEAF_NODES+GN_NUMEL,dev_w,1,
								dev_primal_iterate,1,&primal_infes_iter));
						if(-primal_infes_iter<=dual_gap){
							GPAD_second_cond_flag=1;
							GPAD_primal_inf_term=-primal_infes_iter;
							GPAD_iterates=i;
							i=iterate[0]+1;
						}else{
							primal_cost_function<real_t>(handle);
							if(-primal_infes_iter<=dual_gap/(dual_gap+1)*(prm_cst[0])){
								GPAD_second_cond_flag=2;
								GPAD_primal_inf_term=-primal_infes_iter;
								GPAD_iterates=i;
								i=iterate[0]+1;
							}
						}
					}else{
						primal_cost_function<real_t>(handle);
						if(prm_cst[0]-dual_cst[0]<=dual_gap*(dual_cst[0]>1?dual_cst[0]:1)){
							GPAD_second_cond_flag=3;
							GPAD_primal_inf_term=-primal_infes_iter;
							GPAD_iterates=i;
							i=iterate[0]+1;
						}
					}
				}
			}*/
		}
		if(GPAD_iterates==0){
			ntheta[0]=ntheta[1];
			ntheta[1]=0.5*(sqrt(pow(ntheta[1],4)+4*pow(ntheta[1],2))-pow(ntheta[1],2));
		}else{
			i=iterate[0]+1;
		}
	}
}


template<typename T>__host__ void Mono_GPAD_algorithm(T *x,cublasHandle_t handle){
	real_t alpha;
	real_t primal_infes_iter;
	real_t min_al=-1;
	real_t multiple_prd=1;
	int length=NC*N_NONLEAF_NODES+GN_NUMEL;
	int nthreads=length/2;
	real_t inv_neta=0;
	//iterate[0]
	for(int i=0;i<iterate[0];i++){
		alpha=ntheta[1]*(1/ntheta[0]-1);
		accelerated_dual_update<real_t><<<(N_NONLEAF_NODES+GN_NUMEL),NC>>>(dev_y,&dev_y[N_NONLEAF_NODES*NC+GN_NUMEL],dev_w,alpha);
		_CUDA(cudaMemcpy(dev_q,&dev_w[N_NONLEAF_NODES*NC],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
		_CUDA(cudaMemcpy(dev_lambda,&dev_w[N_NONLEAF_NODES*NC],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
		dynamic_GPAD_Batch<real_t>(ptr_dev_w,ptr_dev_q,ptr_dev_lambda,ptr_dev_s,ptr_dev_x,ptr_dev_u,handle);
		dual_update<real_t>(dev_w,dev_x,dev_u,dev_y,handle);
		_CUBLAS(cublasSdot(handle,N_NONLEAF_NODES*NC+GN_NUMEL,&dev_w[N_NONLEAF_NODES*NC+GN_NUMEL],1,dev_primal_iterate,1,&grad[i]));
		if(grad[i]<0){
			ntheta[1]=1;
			ntheta[0]=1;
			//printf("%d %f ",i, grad[i]);
			_CUDA(cudaMemcpy(dev_y,&dev_y[N_NONLEAF_NODES*NC+GN_NUMEL],(N_NONLEAF_NODES*NC+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToDevice));
			accelerated_dual_update<real_t><<<(N_NONLEAF_NODES+GN_NUMEL),NC>>>(dev_y,&dev_y[N_NONLEAF_NODES*NC+GN_NUMEL],dev_w,0);
			_CUDA(cudaMemcpy(dev_q,&dev_w[N_NONLEAF_NODES*NC],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
			_CUDA(cudaMemcpy(dev_lambda,&dev_w[N_NONLEAF_NODES*NC],GN_NUMEL*sizeof(real_t),cudaMemcpyDeviceToDevice));
			dynamic_GPAD_Batch<real_t>(ptr_dev_w,ptr_dev_q,ptr_dev_lambda,ptr_dev_s,ptr_dev_x,ptr_dev_u,handle);
			dual_update<real_t>(dev_w,dev_x,dev_u,dev_y,handle);
			_CUBLAS(cublasSdot(handle,N_NONLEAF_NODES*NC+GN_NUMEL,&dev_w[N_NONLEAF_NODES*NC+GN_NUMEL],1,dev_primal_iterate,1,&grad[i]));
		}
		//primal_cost_function<real_t>(handle);
		//prm_cst_value[i]=prm_cst[0];
		//dual_cst_value[i]=dual_cst[0];
		if(i>0){
			multiple_prd=1-ntheta[1];
			_CUBLAS(cublasSscal(handle,NC*N_NONLEAF_NODES+GN_NUMEL,&ntheta[1],dev_primal,1));
			_CUBLAS(cublasSaxpy(handle,NC*N_NONLEAF_NODES+GN_NUMEL,&multiple_prd,dev_primal,1,&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],1));
			_CUBLAS(cublasSaxpy(handle,NC*N_NONLEAF_NODES,&min_al,dev_g,1,&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],1));
			_CUBLAS(cublasSaxpy(handle,GN_NUMEL,&min_al,dev_GN,1,&dev_primal[2*NC*N_NONLEAF_NODES+GN_NUMEL],1));
		}else{
			_CUDA(cudaMemcpy(&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],dev_primal,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToDevice));
			_CUBLAS(cublasSaxpy(handle,NC*N_NONLEAF_NODES,&min_al,dev_g,1,&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],1));
			_CUBLAS(cublasSaxpy(handle,GN_NUMEL,&min_al,dev_GN,1,&dev_primal[2*NC*N_NONLEAF_NODES+GN_NUMEL],1));
		}
		find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NC>>>(&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],dev_primal_avg,length,nthreads);
		_CUDA(cudaMemcpy(&primal_infes_iter,dev_primal_avg,sizeof(real_t),cudaMemcpyDeviceToHost));

		inv_neta=1/ntheta[1];
		_CUBLAS(cublasSscal(handle,NC*N_NONLEAF_NODES+GN_NUMEL,&inv_neta,dev_primal,1));
		_CUDA(cudaMemcpy(&dev_primal[NC*N_NONLEAF_NODES+GN_NUMEL],dev_primal,(NC*N_NONLEAF_NODES+GN_NUMEL)*sizeof(real_t),cudaMemcpyDeviceToDevice));

		if(i>5){
			mono_term_condition<real_t>(primal_infes_iter,i,length,nthreads,handle);
		}
		if(GPAD_iterates==0){
			ntheta[0]=ntheta[1];
			ntheta[1]=0.5*(sqrt(pow(ntheta[1],4)+4*pow(ntheta[1],2))-pow(ntheta[1],2));
		}else{
			i=iterate[0]+1;
		}
	}
}
template<typename T>__host__ void primal_cost_function(cublasHandle_t handle){
	prm_cst[0]=0;
	prm_cst[1]=0;
	dual_cst[0]=0;
	dual_cst[1]=0;

	if(NX*N_NODES>NU*N_NONLEAF_NODES){
		_CUDA(cudaMemset(dev_temp_cst,0,NX*N_NODES*sizeof(real_t)));
	}else{
		_CUDA(cudaMemset(dev_temp_cst,0,NU*N_NONLEAF_NODES*sizeof(real_t)));
	}
	_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,nrows[3],1,ncols[3],&al,ptr_dev_Q,
			nrows[3],ptr_dev_x,ncols[3],&bet,ptr_dev_temp_xcst,nrows[3],N_NODES));
	_CUBLAS(cublasSdot(handle,N_NODES*NX,dev_x,1,dev_temp_cst,1,&prm_cst[0]));
	_CUBLAS(cublasSgemmBatched(handle,CUBLAS_OP_N,CUBLAS_OP_N,nrows[4],1,nrows[4],&al,ptr_dev_R,
			nrows[4],ptr_dev_s,nrows[4],&bet,ptr_dev_temp_ucst,nrows[4],N_NONLEAF_NODES));
	_CUBLAS(cublasSdot(handle,N_NONLEAF_NODES*NU,dev_u,1,dev_temp_cst,1,&prm_cst[1]));
	prm_cst[0]=prm_cst[0]+prm_cst[1];
	_CUBLAS(cublasSdot(handle,N_NONLEAF_NODES*NC+GN_NUMEL,dev_y,1,dev_primal_iterate,1,&dual_cst[1]));
	dual_cst[0]=dual_cst[1]+prm_cst[0];
}

template<typename T>__host__ void termination_condition(T primal_infes_iter,int i, int length, int nthreads,cublasHandle_t handle){
	if(primal_infes_iter<primal_inf){
		GPAD_first_cond_flag=1;
		GPAD_iterates=i;
		GPAD_primal_inf_term=primal_infes_iter;
	}else{
		find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NC>>>(dev_primal_iterate,dev_primal_avg,length,nthreads);
		_CUDA(cudaMemcpy(&primal_infes_iter,dev_primal_avg,sizeof(real_t),cudaMemcpyDeviceToHost));
		if(primal_infes_iter<primal_inf){
			_CUBLAS(cublasIsamin(handle,(N_NONLEAF_NODES+GN_NUMEL)*NC,dev_w,1,&index_min));
			_CUDA(cudaMemcpy(&primal_infes_iter,&dev_w[index_min-1],sizeof(real_t),cudaMemcpyDeviceToHost));
			if(primal_infes_iter>=0){
				_CUBLAS(cublasSdot(handle,NC*N_NONLEAF_NODES+GN_NUMEL,dev_w,1,
						dev_primal_iterate,1,&primal_infes_iter));
				if(-primal_infes_iter<=dual_gap){
					GPAD_second_cond_flag=1;
					GPAD_primal_inf_term=-primal_infes_iter;
					GPAD_iterates=i;
				}else{
					primal_cost_function<real_t>(handle);
					real_t gap=dual_gap/(1+dual_gap);
					//printf("%f %f ",-primal_infes_iter,gap*(prm_cst[0]));
					if(-primal_infes_iter<=gap*(prm_cst[0])){
						GPAD_second_cond_flag=2;
						GPAD_primal_inf_term=-primal_infes_iter;
						GPAD_iterates=i;
					}
				}
			}else{
				primal_cost_function<real_t>(handle);
				if(prm_cst[0]-dual_cst[0]<=dual_gap*(dual_cst[0]>1?dual_cst[0]:1)){
					GPAD_second_cond_flag=3;
					GPAD_primal_inf_term=-primal_infes_iter;
					GPAD_iterates=i;
				}
			}
		}
	}
}

template<typename T>__host__ void mono_term_condition(T primal_infes_iter,int i, int length, int nthreads,cublasHandle_t handle){
	if(primal_infes_iter<primal_inf){
		//GPAD_first_cond_flag=1;
		//GPAD_iterates=i;
		//GPAD_primal_inf_term=primal_infes_iter;
		find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NC>>>(dev_primal_iterate,dev_primal_avg,length,nthreads);
		_CUDA(cudaMemcpy(&primal_infes_iter,dev_primal_avg,sizeof(real_t),cudaMemcpyDeviceToHost));
		if(primal_infes_iter<primal_inf){
			_CUBLAS(cublasIsamin(handle,(N_NONLEAF_NODES+GN_NUMEL)*NC,dev_w,1,&index_min));
			_CUDA(cudaMemcpy(&primal_infes_iter,&dev_w[index_min-1],sizeof(real_t),cudaMemcpyDeviceToHost));
			if(primal_infes_iter>=0){
				_CUBLAS(cublasSdot(handle,NC*N_NONLEAF_NODES+GN_NUMEL,dev_w,1,
						dev_primal_iterate,1,&primal_infes_iter));
				if(-primal_infes_iter<=dual_gap){
					GPAD_second_cond_flag=1;
					GPAD_primal_inf_term=-primal_infes_iter;
					GPAD_iterates=i;
				}else{
					primal_cost_function<real_t>(handle);
					real_t gap=dual_gap/(1+dual_gap);
					//printf("%f %f ",-primal_infes_iter,gap*(prm_cst[0]));
					if(-primal_infes_iter<=gap*(prm_cst[0])){
						GPAD_second_cond_flag=2;
						GPAD_primal_inf_term=-primal_infes_iter;
						GPAD_iterates=i;
					}
				}
			}else{
				primal_cost_function<real_t>(handle);
				if(prm_cst[0]-dual_cst[0]<=dual_gap*(dual_cst[0]>1?dual_cst[0]:1)){
					GPAD_second_cond_flag=3;
					GPAD_primal_inf_term=-primal_infes_iter;
					GPAD_iterates=i;
				}
			}
		}
	}else{
		find_max<real_t><<<N_NONLEAF_NODES+GN_NUMEL,NC>>>(dev_primal_iterate,dev_primal_avg,length,nthreads);
		_CUDA(cudaMemcpy(&primal_infes_iter,dev_primal_avg,sizeof(real_t),cudaMemcpyDeviceToHost));
		if(primal_infes_iter<primal_inf){
			_CUBLAS(cublasIsamin(handle,(N_NONLEAF_NODES+GN_NUMEL)*NC,dev_w,1,&index_min));
			_CUDA(cudaMemcpy(&primal_infes_iter,&dev_w[index_min-1],sizeof(real_t),cudaMemcpyDeviceToHost));
			if(primal_infes_iter>=0){
				_CUBLAS(cublasSdot(handle,NC*N_NONLEAF_NODES+GN_NUMEL,dev_w,1,
						dev_primal_iterate,1,&primal_infes_iter));
				if(-primal_infes_iter<=dual_gap){
					GPAD_second_cond_flag=1;
					GPAD_primal_inf_term=-primal_infes_iter;
					GPAD_iterates=i;
				}else{
					primal_cost_function<real_t>(handle);
					real_t gap=dual_gap/(1+dual_gap);
					//printf("%f %f ",-primal_infes_iter,gap*(prm_cst[0]));
					if(-primal_infes_iter<=gap*(prm_cst[0])){
						GPAD_second_cond_flag=2;
						GPAD_primal_inf_term=-primal_infes_iter;
						GPAD_iterates=i;
					}
				}
			}else{
				primal_cost_function<real_t>(handle);
				if(prm_cst[0]-dual_cst[0]<=dual_gap*(dual_cst[0]>1?dual_cst[0]:1)){
					GPAD_second_cond_flag=3;
					GPAD_primal_inf_term=-primal_infes_iter;
					GPAD_iterates=i;
				}
			}
		}
	}
}
int free_GPAD_Batch(){
	if (dev_GPAD_C)
		checkCudaErrors(cudaFree(dev_GPAD_C));
	if (dev_GPAD_D)
		checkCudaErrors(cudaFree(dev_GPAD_D));
	if (dev_GPAD_F)
		checkCudaErrors(cudaFree(dev_GPAD_F));
	if (dev_GPAD_H)
		checkCudaErrors(cudaFree(dev_GPAD_H));
	if (dev_GPAD_K_GAIN)
		checkCudaErrors(cudaFree(dev_GPAD_K_GAIN));
	if (dev_GPAD_PHI)
		checkCudaErrors(cudaFree(dev_GPAD_PHI));
	if (dev_GPAD_SIGMA)
		checkCudaErrors(cudaFree(dev_GPAD_SIGMA));
	if (dev_GPAD_THETA)
		checkCudaErrors(cudaFree(dev_GPAD_THETA));
	if(dev_A)
		_CUDA(cudaFree(dev_A));
	if(dev_B)
		_CUDA(cudaFree(dev_B));
	if(dev_F)
		_CUDA(cudaFree(dev_F));
	if(dev_G)
		_CUDA(cudaFree(dev_G));
	if(dev_FN)
		_CUDA(cudaFree(dev_FN));
	if(dev_g)
		_CUDA(cudaFree(dev_g));
	if(dev_GN)
		_CUDA(cudaFree(dev_GN));
	if(DEV_CONSTANT_TREE_VALUE)
		_CUDA(cudaFree(DEV_CONSTANT_TREE_VALUE));
	if (ptr_dev_GPAD_C)
		_CUDA(cudaFree(ptr_dev_GPAD_C));
	if (ptr_dev_GPAD_D)
		_CUDA(cudaFree(ptr_dev_GPAD_D));
	if (ptr_dev_GPAD_F)
		_CUDA(cudaFree(ptr_dev_GPAD_F));
	if (ptr_dev_GPAD_H)
		_CUDA(cudaFree(ptr_dev_GPAD_H));
	if (ptr_dev_GPAD_K_GAIN)
		_CUDA(cudaFree(ptr_dev_GPAD_K_GAIN));
	if (ptr_dev_GPAD_PHI)
		_CUDA(cudaFree(ptr_dev_GPAD_PHI));
	if (ptr_dev_GPAD_SIGMA)
		_CUDA(cudaFree(ptr_dev_GPAD_SIGMA));
	if (ptr_dev_GPAD_THETA)
		_CUDA(cudaFree(ptr_dev_GPAD_THETA));
	if (ptr_dev_G)
		_CUDA(cudaFree(ptr_dev_G));
	if (ptr_dev_F)
		_CUDA(cudaFree(ptr_dev_F));
	return 0;
}


static inline void verify_ki_1(const size_t k, const size_t i, const char *f, unsigned short l) {
#ifdef GPAD_GUARDS
	if (k > N - 1 || i > TREE_NODES_PER_STAGE[k]) {
#if (GPAD_DEBUG_LEVEL > DEBUG_SILENT)
		fprintf(stderr, "idx(%d,%d) out of range at %s L%d\n", k, i, f,l);
#endif
		exit(323);
	}
#endif
}

static inline void verify_ki_2(const size_t k, const size_t i, const char *f, unsigned short l) {
#ifdef GPAD_GUARDS
	if (k > N  || i > TREE_NODES_PER_STAGE[k]) {
#if (GPAD_DEBUG_LEVEL > DEBUG_SILENT)
		fprintf(stderr, "idx(%d,%d) out of range at %s L%d\n", k, i, f, l);
#endif
		exit(323);
	}
#endif
}

size_t number_of_children(const size_t k, const size_t i) {
	VERIFY_KI1(k,i);
	return TREE_NUM_CHILDREN[TREE_NODES_PER_STAGE_CUMUL[k] + i - 1];
}

size_t idx_tree_value(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	return NX * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
}

size_t idx_c(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	return NX * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
}

size_t idx_sigma(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	return NU * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
}

size_t idx_d(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);/* Auto-generated file */
	return NC * NX * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
}

size_t idx_theta(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	if(k<N-1)
		return NX * NU * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
	else
		return NX*NU*TREE_NODES_PER_STAGE_CUMUL[k]+NU*FN_ROWS_CUMUL[i-1];
}

size_t idx_phi(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	return NC * NU * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
}

size_t idx_h(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	return NX * NU * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
}

size_t idx_f(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	if(k<N-1)
		return NX * NX * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
	else
		return NX*NX*TREE_NODES_PER_STAGE_CUMUL[k]+NX*FN_ROWS_CUMUL[i-1];
}

size_t idx_prob(const size_t k, const size_t i) {
	VERIFY_KI1(k, i);
	return TREE_NODES_PER_STAGE_CUMUL[k] + i - 1;
}

size_t idx_K_gain(const size_t k,const size_t i){
	VERIFY_KI1(k, i);
	return (TREE_NODES_PER_STAGE_CUMUL[k]+i-1)*NU*NX;
}
size_t idx_y(const size_t k, const size_t i) {
	VERIFY_KI2(k, i);
	if (k <= N - 1) {
		return NC * (TREE_NODES_PER_STAGE_CUMUL[k] + i - 1);
	} else {
		return FN_ROWS_CUMUL[i-1] + N_NONLEAF_NODES * NC ;
		//return FN_ROWS_CUMUL[i-1] + N_NONLEAF_NODES * NC - 1;
	}
}

size_t idx_lambda(const size_t k, const size_t i){
	VERIFY_KI2(k, i);
	if(k<N-1){
		return NX*(i-1);
	}
	else{
		return FN_ROWS_CUMUL[i-1];
	}
}

#endif /* DYNAMIC_GPAD_CUH_ */
